truncate table loyalty_bi_analytics.cs_fact_campaign
;
----cs_fact_campaign-----
/*
To insert all facts/Metrics into forecasting sheet as "Forecasting" in to Fact Tables

 */
;



insert into loyalty_bi_analytics.cs_fact_campaign
select *
from loyalty_bi_analytics.cs_fact_campaign_fy19
;
insert into loyalty_bi_analytics.cs_fact_campaign
with aggregated as (
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'super'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,number_of_contacts
,incremental_sales_big_w
,incremental_sales_bws
,incremental_sales_supers
,manual_points_cost_supers
,manual_points_cost_bws
,manual_points_cost_big_w
, manual_points_cost_services
       , manual_points_cost_member_programs
       , manual_points_cost_external
       , "non-manual_points_cost_supers"
       , "non-manual_points_cost_bws"
       , "non-manual_points_cost_big_w"
       , "non-manual_points_cost_services"
       , "non-manual_points_cost_member_programs"
       , "non-manual_points_cost_external"
       , social_media_cost_supers
       , social_media_cost_bws
       , social_media_cost_big_w
       , social_media_cost_services
       , social_media_cost_member_programs
       , social_media_cost_external
       , programmatic_media_cost_supers
       , programmatic_media_cost_bws
       , programmatic_media_cost_big_w
       , programmatic_media_cost_services
       , programmatic_media_cost_member_programs
       , programmatic_media_cost_external
       , direct_mail_cost_supers
       , direct_mail_cost_bws
       , direct_mail_cost_big_w
       , direct_mail_cost_services
       , direct_mail_cost_member_programs
       , direct_mail_cost_external
       , printing_cost_supers
       , printing_cost_bws
       , printing_cost_big_w
       , printing_cost_services
       , printing_cost_member_programs
       , printing_cost_external
       , inventory_cost_supers
       , inventory_cost_bws
       , inventory_cost_big_w
       , inventory_cost_services
       , inventory_cost_member_programs
       , inventory_cost_external
       ,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end promo_post
       , 'super' as source_table2
  from loyalty_bi_analytics.fy20_forecasting_and_planning_supers
    UNION ALL
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'services'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,number_of_contacts
,incremental_sales_big_w
,incremental_sales_bws
,incremental_sales_supers
,manual_points_cost_supers
,manual_points_cost_bws
,manual_points_cost_big_w
, manual_points_cost_services
       , manual_points_cost_member_programs
       , 0
       , "non-manual_points_cost_supers"
       , "non-manual_points_cost_bws"
       , "non-manual_points_cost_big_w"
       , "non-manual_points_cost_services"
       , "non-manual_points_cost_member_programs"
       , 0
       , social_media_cost_supers
       , social_media_cost_bws
       , social_media_cost_big_w
       , social_media_cost_services
       , social_media_cost_member_programs
       , 0
       , programmatic_media_cost_supers
       , programmatic_media_cost_bws
       , programmatic_media_cost_big_w
       , programmatic_media_cost_services
       , programmatic_media_cost_member_programs
       , 0
       , direct_mail_cost_supers
       , direct_mail_cost_bws
       , direct_mail_cost_big_w
       , direct_mail_cost_services
       , direct_mail_cost_member_programs
       , 0
       , printing_cost_supers
       , printing_cost_bws
       , printing_cost_big_w
       , printing_cost_services
       , printing_cost_member_programs
       , 0
       , inventory_cost_supers
       , inventory_cost_bws
       , inventory_cost_big_w
       , inventory_cost_services
       , inventory_cost_member_programs
       , 0
       ,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
       ,'services'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_service
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'member'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,number_of_contacts
,incremental_sales_big_w
,incremental_sales_bws
,incremental_sales_supers
,manual_points_cost_supers
,manual_points_cost_bws
,manual_points_cost_big_w
, manual_points_cost_services
       , manual_points_cost_member_programs
       , 0
       , "non-manual_points_cost_supers"
       , "non-manual_points_cost_bws"
       , "non-manual_points_cost_big_w"
       , "non-manual_points_cost_services"
       , "non-manual_points_cost_member_programs"
       , 0
       , social_media_cost_supers
       , social_media_cost_bws
       , social_media_cost_big_w
       , social_media_cost_services
       , social_media_cost_member_programs
       , 0
       , programmatic_media_cost_supers
       , programmatic_media_cost_bws
       , programmatic_media_cost_big_w
       , programmatic_media_cost_services
       , programmatic_media_cost_member_programs
       , 0
       , direct_mail_cost_supers
       , direct_mail_cost_bws
       , direct_mail_cost_big_w
       , direct_mail_cost_services
       , direct_mail_cost_member_programs
       , 0
       , printing_cost_supers
       , printing_cost_bws
       , printing_cost_big_w
       , printing_cost_services
       , printing_cost_member_programs
       , 0
       , inventory_cost_supers
       , inventory_cost_bws
       , inventory_cost_big_w
       , inventory_cost_services
       , inventory_cost_member_programs
       , 0
,      case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
, 'member'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_members

  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'lcp_cda'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
       , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,       case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
, 'lcp_cda'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_cda
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'bigw'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,number_of_contacts
,incremental_sales_big_w
,incremental_sales_bws
,incremental_sales_supers
,manual_points_cost_supers
,manual_points_cost_bws
,manual_points_cost_big_w
, manual_points_cost_services
        , manual_points_cost_member_programs
       , 0
       , "non-manual_points_cost_supers"
       , "non-manual_points_cost_bws"
       , "non-manual_points_cost_big_w"
       , "non-manual_points_cost_services"
       , "non-manual_points_cost_member_programs"
       , 0
       , social_media_cost_supers
       , social_media_cost_bws
       , social_media_cost_big_w
       , social_media_cost_services
       , social_media_cost_member_programs
       , 0
       , programmatic_media_cost_supers
       , programmatic_media_cost_bws
       , programmatic_media_cost_big_w
       , programmatic_media_cost_services
       , programmatic_media_cost_member_programs
       , 0
       , direct_mail_cost_supers
       , direct_mail_cost_bws
       , direct_mail_cost_big_w
       , direct_mail_cost_services
       , direct_mail_cost_member_programs
       , 0
       , printing_cost_supers
       , printing_cost_bws
       , printing_cost_big_w
       , printing_cost_services
       , printing_cost_member_programs
       , 0
       , inventory_cost_supers
       , inventory_cost_bws
       , inventory_cost_big_w
       , inventory_cost_services
       , inventory_cost_member_programs
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'bigw'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bigw
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'bws'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,number_of_contacts
,incremental_sales_big_w
,incremental_sales_bws
,incremental_sales_supers
,manual_points_cost_supers
,manual_points_cost_bws
,manual_points_cost_big_w
, manual_points_cost_services
           , manual_points_cost_member_programs
       , 0
       , "non-manual_points_cost_supers"
       , "non-manual_points_cost_bws"
       , "non-manual_points_cost_big_w"
       , "non-manual_points_cost_services"
       , "non-manual_points_cost_member_programs"
       , 0
       , social_media_cost_supers
       , social_media_cost_bws
       , social_media_cost_big_w
       , social_media_cost_services
       , social_media_cost_member_programs
       , 0
       , programmatic_media_cost_supers
       , programmatic_media_cost_bws
       , programmatic_media_cost_big_w
       , programmatic_media_cost_services
       , programmatic_media_cost_member_programs
       , 0
       , direct_mail_cost_supers
       , direct_mail_cost_bws
       , direct_mail_cost_big_w
       , direct_mail_cost_services
       , direct_mail_cost_member_programs
       , 0
       , printing_cost_supers
       , printing_cost_bws
       , printing_cost_big_w
       , printing_cost_services
       , printing_cost_member_programs
       , 0
       , inventory_cost_supers
       , inventory_cost_bws
       , inventory_cost_big_w
       , inventory_cost_services
       , inventory_cost_member_programs
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'bws'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'lcp_bau'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
       , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'lcp_bau'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_bau
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
    union all
    select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'bws_blc'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
        , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'bws_blc'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_blc
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
        union all
    select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'bws_de'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
        , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'bws_de'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_de
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
        union all
select
campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream::varchar(20),'caltex'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
        , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'caltex'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_caltex
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
        union all
    select campaign_code,campaign_type,offer_start_date,
  campaign_name_description::varchar(20),offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'cartology'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
       , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'cartology'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_cartology
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
   union all
        select campaign_code,campaign_type,offer_start_date,
  campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,'fuelco'::varchar(20) as source_table
,trunc(financial_week_start_date + 6) as fw_end_date
,max(number_of_contacts)
,sum(incremental_sales_big_w)
,sum(incremental_sales_bws)
,sum(incremental_sales_supers)
,sum(manual_points_cost_supers)
,sum(manual_points_cost_bws)
,sum(manual_points_cost_big_w)
,sum(manual_points_cost_services)
       , sum(manual_points_cost_member_programs)
       , 0
       , sum("non-manual_points_cost_supers")
       , sum("non-manual_points_cost_bws")
       , sum("non-manual_points_cost_big_w")
       , sum("non-manual_points_cost_services")
       , sum("non-manual_points_cost_member_programs")
       , 0
       , sum(social_media_cost_supers)
       , sum(social_media_cost_bws)
       , sum(social_media_cost_big_w)
       , sum(social_media_cost_services)
       , sum(social_media_cost_member_programs)
       , 0
       , sum(programmatic_media_cost_supers)
       , sum(programmatic_media_cost_bws)
       , sum(programmatic_media_cost_big_w)
       , sum(programmatic_media_cost_services)
       , sum(programmatic_media_cost_member_programs)
       , 0
       , sum(direct_mail_cost_supers)
       , sum(direct_mail_cost_bws)
       , sum(direct_mail_cost_big_w)
       , sum(direct_mail_cost_services)
       , sum(direct_mail_cost_member_programs)
       , 0
       , sum(printing_cost_supers)
       , sum(printing_cost_bws)
       , sum(printing_cost_big_w)
       , sum(printing_cost_services)
       , sum(printing_cost_member_programs)
       , 0
       , sum(inventory_cost_supers)
       , sum(inventory_cost_bws)
       , sum(inventory_cost_big_w)
       , sum(inventory_cost_services)
       , sum(inventory_cost_member_programs)
       , 0
,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end  promo_post
,'fuelco'
  from loyalty_bi_analytics.fy20_forecasting_and_planning_fuelco
   group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end

)
, final as (
select
   distinct
  trim(campaign_code) + '_' +trunc(offer_start_date)::text as campaign_id
 ,day_id as fw_end_day_id
 ,number_of_contacts
       ,0::NUMERIC(15,4) as actual_incremental_sales_bws
,0::NUMERIC(15,4) as actual_incremental_sales_big_w
,0::NUMERIC(15,4) as actual_incremental_sales_supers

,0::NUMERIC(15,4) as actual_cost_bws
,0::NUMERIC(15,4) as actual_cost_big_w
,0::NUMERIC(15,4) as actual_cost_supers
,incremental_sales_big_w as forecasting_incremental_sales_big_w
,incremental_sales_bws as forecasting_incremental_sales_bws
,incremental_sales_supers as forecasting_incremental_sales_supers
,manual_points_cost_supers as forecasting_manual_points_cost_supers
,manual_points_cost_bws as forecasting_manual_points_cost_bws
,manual_points_cost_big_w as forecasting_manual_points_cost_big_w
, manual_points_cost_services as forecasting_manual_points_cost_services
       , manual_points_cost_member_programs as forecasting_manual_points_cost_member_programs
       , manual_points_cost_external as forecasting_manual_points_cost_external
       , "non-manual_points_cost_supers" as forecasting_non_manual_points_cost_supers
       , "non-manual_points_cost_bws" as forecasting_non_manual_points_cost_bws
       , "non-manual_points_cost_big_w" as forecasting_non_manual_points_cost_big_w
       , "non-manual_points_cost_services" as forecasting_non_manual_points_cost_services
       , "non-manual_points_cost_member_programs" as forecasting_non_manual_points_cost_member_programs
       , "non-manual_points_cost_external" as forecasting_non_manual_points_cost_external
       , social_media_cost_supers as forecasting_social_media_cost_supers
       , social_media_cost_bws as forecasting_social_media_cost_bws
       , social_media_cost_big_w as forecasting_social_media_cost_big_w
       , social_media_cost_services as forecasting_social_media_cost_services
       , social_media_cost_member_programs as forecasting_social_media_cost_member_programs
       , social_media_cost_external as forecasting_social_media_cost_external
       , programmatic_media_cost_supers as forecasting_programmatic_media_cost_supers
       , programmatic_media_cost_bws as forecasting_programmatic_media_cost_bws
       , programmatic_media_cost_big_w as forecasting_programmatic_media_cost_big_w
       , programmatic_media_cost_services as forecasting_programmatic_media_cost_services
       , programmatic_media_cost_member_programs as forecasting_programmatic_media_cost_member_programs
       , programmatic_media_cost_external as forecasting_programmatic_media_cost_external
       , direct_mail_cost_supers as forecasting_direct_mail_cost_supers
       , direct_mail_cost_bws as forecasting_direct_mail_cost_bws
       , direct_mail_cost_big_w as forecasting_direct_mail_cost_big_w
       , direct_mail_cost_services as forecasting_direct_mail_cost_services
       , direct_mail_cost_member_programs as forecasting_direct_mail_cost_member_programs
       , direct_mail_cost_external as forecasting_direct_mail_cost_external
       , printing_cost_supers as forecasting_printing_cost_supers
       , printing_cost_bws as forecasting_printing_cost_bws
       , printing_cost_big_w as forecasting_printing_cost_big_w
       , printing_cost_services as forecasting_printing_cost_services
       , printing_cost_member_programs as forecasting_printing_cost_member_programs
       , printing_cost_external as forecasting_printing_cost_external
       , inventory_cost_supers as forecasting_inventory_cost_supers
       , inventory_cost_bws as forecasting_inventory_cost_bws
       , inventory_cost_big_w as forecasting_inventory_cost_big_w
       , inventory_cost_services as forecasting_inventory_cost_services
       , inventory_cost_member_programs as forecasting_inventory_cost_member_programs
       , inventory_cost_external as forecasting_inventory_cost_external
       ,promo_post
       ,a.source_table
--into loyalty_bi_analytics.cs_dim_campaign
from aggregated a

inner join loyalty_bi_analytics.cs_dim_date g
  on a.fw_end_date = g.fw_end_date
where offer_start_date is not null
       and len(a.campaign_code)>2

UNION ALL

  select
    distinct
  campaign_code + '_' +trunc(offer_start_date)::text as campaign_id
 ,day_id as fw_end_day_id
,number_of_contacts
,0::NUMERIC(15,4) as actual_incremental_sales_bws
,0::NUMERIC(15,4) as actual_incremental_sales_big_w
,0::NUMERIC(15,4) as actual_incremental_sales_supers

,0::NUMERIC(15,4) as actual_cost_bws
,0::NUMERIC(15,4) as actual_cost_big_w
,0::NUMERIC(15,4) as actual_cost_supers

,incremental_sales_big_w as forecasting_incremental_sales_big_w
,incremental_sales_bws as forecasting_incremental_sales_bws
,incremental_sales_supers as forecasting_incremental_sales_supers
,manual_points_cost_supers as forecasting_manual_points_cost_supers
,manual_points_cost_bws as forecasting_manual_points_cost_bws
,manual_points_cost_big_w as forecasting_manual_points_cost_big_w
, manual_points_cost_services as forecasting_manual_points_cost_services
       , manual_points_cost_member_programs as forecasting_manual_points_cost_member_programs
       , 0 as forecasting_manual_points_cost_external
       , "non-manual_points_cost_services" as forecasting_non_manual_points_cost_services
       , "non-manual_points_cost_member_programs" as forecasting_non_manual_points_cost_member_programs
       , 0 as forecasting_non_manual_points_cost_external
       , "non-manual_points_cost_supers" as forecasting_non_manual_points_cost_supers
       , "non-manual_points_cost_bws" as forecasting_non_manual_points_cost_bws
       , "non-manual_points_cost_big_w" as forecasting_non_manual_points_cost_big_w
       , social_media_cost_supers as forecasting_social_media_cost_supers
       , social_media_cost_bws as forecasting_social_media_cost_bws
       , social_media_cost_big_w as forecasting_social_media_cost_big_w
       , social_media_cost_services as forecasting_social_media_cost_services
       , social_media_cost_member_programs as forecasting_social_media_cost_member_programs
       , 0 as forecasting_social_media_cost_external
       , programmatic_media_cost_supers as forecasting_programmatic_media_cost_supers
       , programmatic_media_cost_bws as forecasting_programmatic_media_cost_bws
       , programmatic_media_cost_big_w as forecasting_programmatic_media_cost_big_w
       , programmatic_media_cost_services as forecasting_programmatic_media_cost_services
       , programmatic_media_cost_member_programs as forecasting_programmatic_media_cost_member_programs
       , 0 as forecasting_programmatic_media_cost_external
       , direct_mail_cost_supers as forecasting_direct_mail_cost_supers
       , direct_mail_cost_bws as forecasting_direct_mail_cost_bws
       , direct_mail_cost_big_w as forecasting_direct_mail_cost_big_w
       , direct_mail_cost_services as forecasting_direct_mail_cost_services
       , direct_mail_cost_member_programs as forecasting_direct_mail_cost_member_programs
       , 0 as forecasting_direct_mail_cost_external
       , printing_cost_supers as forecasting_printing_cost_supers
       , printing_cost_bws as forecasting_printing_cost_bws
       , printing_cost_big_w as forecasting_printing_cost_big_w
       , printing_cost_services as forecasting_printing_cost_services
       , printing_cost_member_programs as forecasting_printing_cost_member_programs
       , 0 as forecasting_printing_cost_external
       , inventory_cost_supers as forecasting_inventory_cost_supers
       , inventory_cost_bws as forecasting_inventory_cost_bws
       , inventory_cost_big_w as forecasting_inventory_cost_big_w
       , inventory_cost_services as forecasting_inventory_cost_services
       , inventory_cost_member_programs as forecasting_inventory_cost_member_programs
       , 0 as forecasting_inventory_cost_external
       , case when promo_post in ('Promo','Post') then lower(promo_post) else 'promo' end
       , 'cross_banner'
from loyalty_bi_analytics.fy20_forecasting_and_planning_cross_banner a
        inner join loyalty_bi_analytics.cs_dim_date g
       on trunc(a.financial_week_start_date+6) = g.fw_end_date
where offer_start_date is not null
       and len(a.campaign_code)>2
)

select a.campaign_id, fw_end_day_id, number_of_contacts, actual_incremental_sales_bws, actual_incremental_sales_big_w, actual_incremental_sales_supers, actual_cost_bws, actual_cost_big_w, actual_cost_supers, forecasting_incremental_sales_big_w, forecasting_incremental_sales_bws, forecasting_incremental_sales_supers, forecasting_manual_points_cost_supers, forecasting_manual_points_cost_bws, forecasting_manual_points_cost_big_w, forecasting_manual_points_cost_services, forecasting_manual_points_cost_member_programs, forecasting_manual_points_cost_external, forecasting_non_manual_points_cost_supers, forecasting_non_manual_points_cost_bws, forecasting_non_manual_points_cost_big_w, forecasting_non_manual_points_cost_services, forecasting_non_manual_points_cost_member_programs, forecasting_non_manual_points_cost_external, forecasting_social_media_cost_supers, forecasting_social_media_cost_bws, forecasting_social_media_cost_big_w, forecasting_social_media_cost_services, forecasting_social_media_cost_member_programs, forecasting_social_media_cost_external, forecasting_programmatic_media_cost_supers, forecasting_programmatic_media_cost_bws, forecasting_programmatic_media_cost_big_w, forecasting_programmatic_media_cost_services, forecasting_programmatic_media_cost_member_programs, forecasting_programmatic_media_cost_external, forecasting_direct_mail_cost_supers, forecasting_direct_mail_cost_bws, forecasting_direct_mail_cost_big_w, forecasting_direct_mail_cost_services, forecasting_direct_mail_cost_member_programs, forecasting_direct_mail_cost_external, forecasting_printing_cost_supers, forecasting_printing_cost_bws, forecasting_printing_cost_big_w, forecasting_printing_cost_services, forecasting_printing_cost_member_programs, forecasting_printing_cost_external, forecasting_inventory_cost_supers, forecasting_inventory_cost_bws, forecasting_inventory_cost_big_w, forecasting_inventory_cost_services, forecasting_inventory_cost_member_programs, forecasting_inventory_cost_external, promo_post,null,0,0,0,0,0,0,0,0,0,0,0,0,0,'Forecasting',0,0,0,0,0,source_table
from final a
;


update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'super'
from loyalty_bi_analytics.fy20_forecasting_and_planning_supers b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'services'
from loyalty_bi_analytics.fy20_forecasting_and_planning_service b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'bws'
from loyalty_bi_analytics.fy20_forecasting_and_planning_bws b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'bws_blc'
from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_blc b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'member'
from loyalty_bi_analytics.fy20_forecasting_and_planning_members b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';


update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'cross_banner'
from loyalty_bi_analytics.fy20_forecasting_and_planning_cross_banner b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'lcp_cda'
from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_cda b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';


update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'cartology'
from loyalty_bi_analytics.fy20_forecasting_and_planning_cartology b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';


update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'lcp_bau'
from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_bau b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';


update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'bigw'
from loyalty_bi_analytics.fy20_forecasting_and_planning_bigw b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'fuelco'
from loyalty_bi_analytics.fy20_forecasting_and_planning_fuelco b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual';

update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'caltex'
from loyalty_bi_analytics.fy20_forecasting_and_planning_caltex b
where b.campaign_code+'_'+trunc(b.offer_start_date) = cs_dim_campaign.campaign_id
and cs_dim_campaign.source_table = 'Manual'
;

------- cs_dim_campaign  -------
insert into loyalty_bi_analytics.cs_dim_campaign
with aggregated as (
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page
, max(landing_pages::int)::BOOLEAN as landing_pages,max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream
       ,'super'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_supers
       group by 1,2,3,5,6
    UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
     ,max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream
       ,'services'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_service
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
      , max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream,'member'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_members
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
      , max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream,'lcp_cda'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_cda
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
       ,max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream,'bigw'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bigw
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
    , max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat ,max(banner) as banner,max(stream) as stream ,'bws'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream ,'lcp_bau'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_bau
 group by 1,2,3,5,6

      UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream ,'bws_blc'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_blc
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream ,'bws_de'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_de
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream)::varchar as stream ,'caltex'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_caltex
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description)::varchar as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream)::varchar  as stream ,'cartology'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_cartology
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream)::varchar as stream ,'fuelco'::varchar(20) as source_table
  from loyalty_bi_analytics.fy20_forecasting_and_planning_fuelco
 group by 1,2,3,5,6
    )


        ,cross_banner as (select campaign_code,campaign_type,offer_start_date,
   campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,search,youtube,gmail,snapchat,'cross_banner'::varchar(20) as source_table
 ,max(incremental_sales_supers) as incremental_sales_supers, max(incremental_sales_bws) as incremental_sales_bws, max(incremental_sales_big_w) as incremental_sales_big_w
   from loyalty_bi_analytics.fy20_forecasting_and_planning_cross_banner
 group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25 )


, final as (
select
   distinct trim(campaign_code) + '_' +trunc(offer_start_date)::text as campaign_id
  ,campaign_code::text
  ,offer_start_date
  ,case when len(campaign_name_description)< 2 then a.campaign_type + ' ' + offer_start_date else campaign_name_description end::text as campaign_name_description
  ,CEILING (case when (trunc(offer_end_date)-trunc(offer_start_date)) < 7 and (trunc(offer_end_date)-trunc(offer_start_date))>=0 then 7  else (trunc(offer_end_date +1)-trunc(offer_start_date)) end / 7) campaign_duration
  ,nvl(CEILING ((trunc(post_period_end_date)-trunc(offer_end_date))/7),0) as post_campaign_duration
  ,nvl(b.channel_id,0)
  ,nvl(c.division_id,16)
  ,nvl(d.stream_id,1)
  ,campaign_type_id
  ,a.source_table::text
--into loyalty_bi_analytics.cs_dim_campaign
from aggregated a
  left join loyalty_bi_analytics.cs_dim_channel b
  on b.email = a.email
  and b.social_facebook_paid = a.social_facebook_paid
  and b.app = a.app
  and b.digital_display = a.digital_display
  and b.digital_wallet = a.digital_wallet
  and b.dm = a.dm
  and b.docket = a.docket
  and b.instagram = a.instagram
  and b.sms = a.sms
  and b.rewards_hub = a.rewards_hub
  and b.social_facebook_owned_page = a.social_facebook_owned_page
  and b.landing_pages = a.landing_pages
  and b.search = a.search
  and b.youtube = a.youtube
  and b.gmail = a.gmail
  and b.snapchat = a.snapchat
  left join loyalty_bi_analytics.cs_dim_division c
  on case when lower(a.banner) = 'supermarkets' then True else False end  = c.supermarket
  and case when lower(a.banner) = 'bws' then True else False end = c.bws
  and case when lower(a.banner) = 'big w' then True else False end = c.bigw
  and case when lower(a.banner) = 'services' then True else False end = c.services
  and case when lower(a.banner) = 'member programs' then True else False end = c.member_program
  left join loyalty_bi_analytics.cs_dim_stream d
  on a.stream = d.stream
  left join loyalty_bi_analytics.cs_dim_campaign_type e
  on a.campaign_type = e.campaign_type
where offer_start_date is not null
       and len(a.campaign_code)>2
UNION ALL
  select
   distinct campaign_code + '_' + trunc(offer_start_date) as campaign_id
  ,campaign_code
  ,offer_start_date
  ,case when len(campaign_name_description)< 2 then a.campaign_type + ' ' + offer_start_date else campaign_name_description end as campaign_name_description
  ,CEILING (case when (trunc(offer_end_date)-trunc(offer_start_date)) < 7 and (trunc(offer_end_date)-trunc(offer_start_date))>=0 then 7  else (trunc(offer_end_date +1)-trunc(offer_start_date)) end / 7) campaign_duration
  ,nvl(CEILING ((trunc(post_period_end_date)-trunc(offer_end_date))/7),0) as post_campaign_duration
  ,nvl(b.channel_id,0)
  ,nvl(c.division_id,16)
  ,nvl(d.stream_id,1)
  ,e.campaign_type_id
  ,'cross_banner' as source_table
from cross_banner a
  left join loyalty_bi_analytics.cs_dim_channel b
  on b.email = a.email
  and b.social_facebook_paid = a.social_facebook_paid
  and b.app = a.app
  and b.digital_display = a.digital_display
  and b.digital_wallet = a.digital_wallet
  and b.dm = a.dm
  and b.docket = a.docket
  and b.instagram = a.instagram
  and b.sms = a.sms
  and b.rewards_hub = a.rewards_hub
  and b.social_facebook_owned_page = a.social_facebook_owned_page
  and b.landing_pages = a.landing_pages
  and b.search = a.search
  and b.youtube = a.youtube
  and b.gmail = a.gmail
  and b.snapchat = a.snapchat
  left join loyalty_bi_analytics.cs_dim_division c
  on case when a.incremental_sales_supers>0 then True else False end  = c.supermarket
  and case when a.incremental_sales_bws>0 then True else False end = c.bws
  and case when a.incremental_sales_big_w>0 then True else False end = c.bigw
  and False = c.services
  and False = c.member_program
  left join loyalty_bi_analytics.cs_dim_stream d
  on a.stream = d.stream
  left join loyalty_bi_analytics.cs_dim_campaign_type e
  on a.campaign_type = e.campaign_type
where offer_start_date is not null
       and len(a.campaign_code)>2
)

select *
from final
where campaign_id not in (select distinct campaign_id from loyalty_bi_analytics.cs_dim_campaign)

;

-- select *
-- from loyalty_bi_analytics.cs_dim_campaign
-- where campaign_code like 'CNA%'
-- and offer_start_date >= '2019-07-01'

;
update loyalty_bi_analytics.cs_dim_campaign
set campaign_type_id = a.campaign_type_id,
    division_id = a.division_id
from (with aggregated as (
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page
, max(landing_pages::int)::BOOLEAN as landing_pages,max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream
       ,'super'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_supers
       group by 1,2,3,5,6
    UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
     ,max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream
       ,'services'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_service
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
      , max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(case when campaign_name_description like '%BWS%' then 'BWS' when campaign_name_description like '%BIG W%' then 'BIG W' when campaign_name_description like '%Fuel%' then 'FuelCo' else 'Supermarkets' end ) as banner,max(stream) as stream,'member'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_members
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
      , max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream,'lcp_cda'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_cda
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
       ,max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream,'bigw'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bigw
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
    , max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat ,max(banner) as banner,max(stream) as stream ,'bws'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws
        group by 1,2,3,5,6
  UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream ,'lcp_bau'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_lcp_bau
 group by 1,2,3,5,6

      UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream ,'bws_blc'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_blc
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream) as stream ,'bws_de'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_bws_de
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream)::varchar as stream ,'caltex'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_caltex
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description)::varchar as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max('Supermarkets') as banner,max(stream)::varchar  as stream ,'cartology'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_cartology
 group by 1,2,3,5,6
          UNION ALL
select campaign_code,campaign_type,offer_start_date,
  max(campaign_name_description) as campaign_name_description,offer_end_date,post_period_end_date,max(email::int)::BOOLEAN as email ,max(social_facebook_paid::int)::BOOLEAN as social_facebook_paid,max(app::int)::BOOLEAN as app,max(digital_display::int)::BOOLEAN as digital_display
,max(digital_wallet::int)::BOOLEAN as digital_wallet ,max(dm::int)::BOOLEAN as dm,max(docket::int)::BOOLEAN as docket,max(instagram::int)::BOOLEAN as instagram,max(sms::int)::BOOLEAN as sms,max(rewards_hub::int)::BOOLEAN as rewards_hub,max(social_facebook_owned_page::int)::BOOLEAN as social_facebook_owned_page , max(landing_pages::int)::BOOLEAN as landing_pages
,      max(search::int)::BOOLEAN as search, max(youtube::int)::BOOLEAN  as youtube, max(gmail::int)::BOOLEAN  as gmail, max(snapchat::int)::BOOLEAN  as snapchat,max(banner) as banner,max(stream)::varchar as stream ,'fuelco'::varchar(20) as source_table,max(execution_type) as execution_type
  from loyalty_bi_analytics.fy20_forecasting_and_planning_fuelco
 group by 1,2,3,5,6
    )


        ,cross_banner as (select campaign_code,campaign_type,offer_start_date,
   campaign_name_description,offer_end_date,post_period_end_date,email,social_facebook_paid,app,digital_display
,digital_wallet,dm,docket,instagram,sms,rewards_hub,social_facebook_owned_page,landing_pages,banner,stream,search,youtube,gmail,snapchat,'cross_banner'::varchar(20) as source_table
 ,max(incremental_sales_supers) as incremental_sales_supers, max(incremental_sales_bws) as incremental_sales_bws, max(incremental_sales_big_w) as incremental_sales_big_w,max(execution_type) as execution_type
   from loyalty_bi_analytics.fy20_forecasting_and_planning_cross_banner
 group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25 )


, final as (
select
   distinct trim(campaign_code) + '_' +trunc(offer_start_date)::text as campaign_id
  ,campaign_code::text
  ,offer_start_date
  ,case when len(campaign_name_description)< 2 then a.campaign_type + ' ' + offer_start_date else campaign_name_description end::text as campaign_name_description
  ,CEILING (case when (trunc(offer_end_date)-trunc(offer_start_date)) < 7 and (trunc(offer_end_date)-trunc(offer_start_date))>=0 then 7  else (trunc(offer_end_date +1)-trunc(offer_start_date)) end / 7) campaign_duration
  ,nvl(CEILING ((trunc(post_period_end_date)-trunc(offer_end_date))/7),0) as post_campaign_duration
  ,nvl(b.channel_id,0)
  ,nvl(c.division_id,16) as division_id
  ,nvl(d.stream_id,1)
  ,nvl(e.campaign_type_id,1) campaign_type_id
  ,a.source_table::text
--into loyalty_bi_analytics.cs_dim_campaign
from aggregated a
  left join loyalty_bi_analytics.cs_dim_channel b
  on b.email = a.email
  and b.social_facebook_paid = a.social_facebook_paid
  and b.app = a.app
  and b.digital_display = a.digital_display
  and b.digital_wallet = a.digital_wallet
  and b.dm = a.dm
  and b.docket = a.docket
  and b.instagram = a.instagram
  and b.sms = a.sms
  and b.rewards_hub = a.rewards_hub
  and b.social_facebook_owned_page = a.social_facebook_owned_page
  and b.landing_pages = a.landing_pages
  and b.search = a.search
  and b.youtube = a.youtube
  and b.gmail = a.gmail
  and b.snapchat = a.snapchat
  left join loyalty_bi_analytics.cs_dim_division c
  on case when lower(a.banner) = 'supermarkets' then True else False end  = c.supermarket
  and case when lower(a.banner) = 'bws' then True else False end = c.bws
  and case when lower(a.banner) = 'big w' then True else False end = c.bigw
  and case when lower(a.banner) = 'services' then True else False end = c.services
  and case when lower(a.banner) = 'member programs' then True else False end = c.member_program
  and case when lower(a.banner) = 'caltex' then True else False end = c.caltex
  and case when lower(a.banner) = 'fuelco' then True else False end = c.fuelco
  left join loyalty_bi_analytics.cs_dim_stream d
  on a.stream = d.stream
  left join loyalty_bi_analytics.cs_dim_campaign_type e
  on a.execution_type = e.campaign_type
where offer_start_date is not null
       and len(a.campaign_code)>2
UNION ALL
  select
   distinct campaign_code + '_' + trunc(offer_start_date) as campaign_id
  ,campaign_code
  ,offer_start_date
  ,case when len(campaign_name_description)< 2 then a.campaign_type + ' ' + offer_start_date else campaign_name_description end as campaign_name_description
  ,CEILING (case when (trunc(offer_end_date)-trunc(offer_start_date)) < 7 and (trunc(offer_end_date)-trunc(offer_start_date))>=0 then 7  else (trunc(offer_end_date +1)-trunc(offer_start_date)) end / 7) campaign_duration
  ,nvl(CEILING ((trunc(post_period_end_date)-trunc(offer_end_date))/7),0) as post_campaign_duration
  ,nvl(b.channel_id,0)
  ,nvl(c.division_id,16)
  ,nvl(d.stream_id,1)
  ,nvl(e.campaign_type_id,1)
  ,'cross_banner' as source_table
from cross_banner a
  left join loyalty_bi_analytics.cs_dim_channel b
  on b.email = a.email
  and b.social_facebook_paid = a.social_facebook_paid
  and b.app = a.app
  and b.digital_display = a.digital_display
  and b.digital_wallet = a.digital_wallet
  and b.dm = a.dm
  and b.docket = a.docket
  and b.instagram = a.instagram
  and b.sms = a.sms
  and b.rewards_hub = a.rewards_hub
  and b.social_facebook_owned_page = a.social_facebook_owned_page
  and b.landing_pages = a.landing_pages
  and b.search = a.search
  and b.youtube = a.youtube
  and b.gmail = a.gmail
  and b.snapchat = a.snapchat
  left join loyalty_bi_analytics.cs_dim_division c
  on True = c.supermarket
  and case when a.incremental_sales_bws>0 then True else False end = c.bws
  and case when a.incremental_sales_big_w>0 then True else False end = c.bigw
  and False = c.services
  and False = c.member_program
  and False = c.fuelco
  and false = c.caltex
  left join loyalty_bi_analytics.cs_dim_stream d
  on a.stream = d.stream
  left join loyalty_bi_analytics.cs_dim_campaign_type e
  on a.execution_type = e.campaign_type
where offer_start_date is not null
       and len(a.campaign_code)>2
)

select *
from final) a
where a.campaign_id = cs_dim_campaign.campaign_id
;
-- select c.division_description,count(distinct b.campaign_id)
-- from loyalty_bi_analytics.cs_dim_campaign a
-- inner join loyalty_bi_analytics.cs_fact_campaign b
-- on a.campaign_id = b.campaign_id
-- inner join loyalty_bi_analytics.cs_dim_division c
-- on a.division_id = c.division_id
-- where b.entry_type = 'Forecasting'
-- and a.campaign_count_flag = True
-- and a.offer_start_date>= '2019-07-01'
-- group by c.division_description;
-- ;DELETE from loyalty_bi_analytics.cs_dim_campaign
-- where campaign_id not in (select distinct campaign_id from loyalty_bi_analytics.cs_fact_campaign)
-- ;
-- ;
-- -- update loyalty_bi_analytics.cs_fact_campaign
-- --         set actual_incremental_sales_bws =actual_inc_sales.bws_incs_sales::NUMERIC(15,4),
-- --          actual_incremental_sales_big_w =actual_inc_sales.bigw_inc_sales::NUMERIC(15,4),
--           actual_incremental_sales_supers =actual_inc_sales.super_inc_sales::NUMERIC(15,4)
-- from (
--  select nvl(nvl(a.campaign_code,b.campaign_code),c.campaign_code) as campaign_code,
--   case when nvl(nvl(a.offer_exec_start_date,b.offer_exec_start_date),c.offer_exec_start_date) is null then nvl(a.fw_end_date,b.fw_end_date) else nvl(nvl(a.offer_exec_start_date,b.offer_exec_start_date),c.offer_exec_start_date) end as offer_exec_start_date,
--   nvl(a.inc_sales,0) as super_inc_sales,
--   nvl(b.inc_sales,0) as bws_incs_sales,
--   nvl(c.inc_sales,0) as bigw_inc_sales,
--   nvl(nvl(a.fw_end_date,b.fw_end_date),c.fw_end_date) fw_end_date,
--   nvl(nvl(a.target_count,b.target_count),c.target_count) as number_of_contact,
--   nvl(nvl(a.input_type,b.input_type),c.input_type) as input_type
--   ,case when a.campaign_code is not null then 'Super' when b.campaign_code is not null then 'BWS' when c.campaign_code is not null then 'BIGW' end as source_type
-- from (select fw_end_date,campaign_code,campaign_name,offer_exec_start_date,input_type
-- ,sum(inc_sales) as inc_sales,max(target_count) as target_count
-- from  loyalty_bi_analytics.vw_inc_sales_wkly_rprt
-- group by 1,2,3,4,5) a
--
-- FULL OUTER JOIN  (select fw_end_date,campaign_code,campaign_name,offer_exec_start_date,input_type
-- ,sum(inc_sales) as inc_sales,max(target_count) as target_count
-- from  loyalty_bi_analytics.vw_inc_sales_wkly_rprt_bws
-- group by 1,2,3,4,5) b
--   on a.fw_end_date = b.fw_end_date
--   and a.campaign_code = b.campaign_code
--   and a.offer_exec_start_date = b.offer_exec_start_date
-- full outer join (select fw_end_date,campaign_code,campaign_name,offer_exec_start_date,input_type
-- ,sum(inc_sales) as inc_sales,max(target_count) as target_count
-- from  loyalty_bi_analytics.vw_inc_sales_wkly_rprt_bigw
-- group by 1,2,3,4,5) c
--   on a.fw_end_date = c.fw_end_date
--   and a.campaign_code = c.campaign_code
--   and a.offer_exec_start_date = c.offer_exec_start_date
--         ) actual_inc_sales
--
--   inner join loyalty_bi_analytics.cs_dim_date cdd
--        on actual_inc_sales.fw_end_date = cdd.fw_end_date
-- where cs_fact_campaign.fw_end_day_id = cdd.day_id
-- and split_part(cs_fact_campaign.campaign_id,'_',1) = actual_inc_sales.campaign_code
-- and split_part(cs_fact_campaign.campaign_id,'_',2) = actual_inc_sales.offer_exec_start_date
-- and entry_type = 'Forecasting';
-- ;
--
-- update loyalty_bi_analytics.cs_fact_campaign
--         set actual_cost_bws = actual_cost.bws_actual_cost::NUMERIC(15,4),
--           actual_cost_big_w = actual_cost.bigw_actual_cost::NUMERIC(15,4),
--           actual_cost_supers = actual_cost.super_actual_cost::NUMERIC(15,4)
-- from ( with a as (
--
--         SELECT
--           nvl(nvl(super.campaign_code, bws.campaign_code), bigw.campaign_code)                            AS campaign_code,
--           CASE WHEN nvl(nvl(super.campaign_start_date, bws.campaign_start_date), bigw.campaign_start_date) IS NULL
--             THEN nvl(super.fw_end_date, bws.fw_end_date)
--           ELSE nvl(nvl(super.campaign_start_date, bws.campaign_start_date),
--                    bigw.campaign_start_date) END                                                          AS offer_exec_start_date,
--           nvl(nvl(super.fw_end_date, bws.fw_end_date),
--               bigw.fw_end_date)                                                                           AS fw_end_date,
--           nvl(super.actual_cost,
--               0)                                                                                          AS super_actual_cost,
--           nvl(bws.actual_cost,
--               0)                                                                                          AS bws_actual_cost,
--           nvl(bigw.actual_cost,
--               0)                                                                                          AS bigw_actual_cost
--         FROM (
--                SELECT
--                  fw_end_date,
--                  dc_campaign_code       AS campaign_code,
--                  dc_campaign_start_date AS campaign_start_date,
--                  sum(cost)              AS actual_cost
--                FROM loyalty_bi_analytics.cd_campaign_summary_report_manual
--                WHERE dc_campaign_start_date >= '2018-05-01'
--                GROUP BY 1, 2, 3) super
--
--           FULL OUTER JOIN (
--                             SELECT
--                               fw_end_date,
--                               dc_campaign_code       AS campaign_code,
--                               dc_campaign_start_date AS campaign_start_date,
--                               sum(cost)              AS actual_cost
--                             FROM loyalty_bi_analytics.cd_campaign_summary_report_manual_bws
--                             WHERE dc_campaign_start_date >= '2018-05-01'
--                             GROUP BY 1, 2, 3) bws
--             ON super.campaign_code = bws.campaign_code
--                AND super.campaign_start_date = bws.campaign_code
--                AND super.fw_end_date = bws.fw_end_date
--           FULL OUTER JOIN
--           (
--             SELECT
--               fw_end_date,
--               dc_campaign_code       AS campaign_code,
--               dc_campaign_start_date AS campaign_start_date,
--               sum(cost)              AS actual_cost
--             FROM loyalty_bi_analytics.cd_campaign_summary_report_manual_bigw
--             WHERE dc_campaign_start_date >= '2018-05-01'
--             GROUP BY 1, 2, 3) bigw
--             ON super.campaign_code = bigw.campaign_code
--                AND super.campaign_start_date = bigw.campaign_code
--                AND super.fw_end_date = bigw.fw_end_date
-- )
-- select cdd.day_id,a.campaign_code+'_'+a.offer_exec_start_date as campaign_id,a.bigw_actual_cost,a.bws_actual_cost,a.super_actual_cost
-- from a inner join loyalty_bi_analytics.cs_dim_date cdd
--   on a.fw_end_date = cdd.fw_end_date) actual_cost
-- where cs_fact_campaign.fw_end_day_id = actual_cost.day_id
-- and cs_fact_campaign.campaign_id = actual_cost.campaign_id
-- and entry_type = 'Forecasting';


;
;
update loyalty_bi_analytics.cs_fact_campaign
        set actual_cost_bws = 0,
          actual_cost_big_w = 0,
          actual_cost_supers = 0,
          actual_incremental_sales_supers = 0,
          actual_incremental_sales_big_w = 0,
actual_incremental_sales_bws = 0
from loyalty_bi_analytics.cs_dim_date b
where cs_fact_campaign.fw_end_day_id = b.day_id
and b.fy = 2020;


update loyalty_bi_analytics.cs_fact_campaign
        set campaign_counter = update_info.campaign_id
from (select campaign_id,campaign_counter,row_number() over (PARTITION BY campaign_id,cdd.fy order by cdd.fw_end_date) as ro_no,fw_end_day_id
from loyalty_bi_analytics.cs_fact_campaign a
     inner join loyalty_bi_analytics.cs_dim_date cdd
     on a.fw_end_day_id = cdd.day_id) update_info
where cs_fact_campaign.campaign_id = update_info.campaign_id
and cs_fact_campaign.fw_end_day_id= update_info.fw_end_day_id
and ro_no = 1
and cs_fact_campaign.entry_type = 'Forecasting';

update loyalty_bi_analytics.cs_fact_campaign
        set forecasting_manual_points_cost_all = nvl(forecasting_manual_points_cost_big_w,0)+ nvl(forecasting_manual_points_cost_bws,0) + nvl(forecasting_manual_points_cost_supers,0)+nvl(forecasting_manual_points_cost_member_programs,0)+nvl(forecasting_manual_points_cost_services)
              , forecasting_programmatic_media_cost_all = nvl(forecasting_programmatic_media_cost_big_w,0)+ nvl(forecasting_programmatic_media_cost_bws,0) + nvl(forecasting_programmatic_media_cost_supers,0)+nvl(forecasting_programmatic_media_cost_member_programs,0)+nvl(forecasting_programmatic_media_cost_services)
              , forecasting_non_manual_points_cost_all = nvl(forecasting_non_manual_points_cost_big_w,0)+ nvl(forecasting_non_manual_points_cost_bws,0) + nvl(forecasting_non_manual_points_cost_supers,0)+nvl(forecasting_non_manual_points_cost_member_programs,0)+nvl(forecasting_non_manual_points_cost_services)
              , forecasting_printing_cost_all = nvl(forecasting_printing_cost_big_w,0)+ nvl(forecasting_printing_cost_bws,0) + nvl(forecasting_printing_cost_supers,0)+nvl(forecasting_printing_cost_member_programs,0)+nvl(forecasting_printing_cost_services)
              , forecasting_direct_mail_cost_all = nvl(forecasting_direct_mail_cost_big_w,0)+ nvl(forecasting_direct_mail_cost_bws,0) + nvl(forecasting_direct_mail_cost_supers,0)+nvl(forecasting_direct_mail_cost_member_programs,0)+nvl(forecasting_direct_mail_cost_services)
              , forecasting_inventory_cost_all = nvl(forecasting_inventory_cost_big_w,0)+ nvl(forecasting_inventory_cost_bws,0) + nvl(forecasting_inventory_cost_supers,0)+nvl(forecasting_inventory_cost_member_programs,0)+nvl(forecasting_inventory_cost_services);

;


truncate table loyalty_bi_analytics.cs_fact_campaign_weekly_forecasting;

insert into loyalty_bi_analytics.cs_fact_campaign_weekly_forecasting
        with a as
(
select d.day_id
       ,16 as division_id
       , a.forecast_incremental_sales as forecast_inc_sales
       ,a.budget_incremental_sales as budget_inc_sales

from loyalty_bi_analytics.cd_2018_forecast_jh a
inner join loyalty_bi_analytics.cs_dim_date d
       on a.fw_end_date = d.fw_end_date
union all
select d.day_id
       ,24
       , a.forecast_incremental_sales as forecast_inc_sales
       ,a.budget_incremental_sales as budget_inc_sale
from loyalty_bi_analytics.cd_2018_forecast_jh_bws a
inner join loyalty_bi_analytics.cs_dim_date d
       on a.fw_end_date = d.fw_end_date
UNION all
        select d.day_id
       ,28
       , a.forecast_incremental_sales as forecast_inc_sales
       ,a.budget_incremental_sales as budget_inc_sale
from loyalty_bi_analytics.cd_2018_forecast_jh_bigw a
inner join loyalty_bi_analytics.cs_dim_date d
       on a.fw_end_date = d.fw_end_date)
        select *
       from a
;

;



-- update loyalty_bi_analytics.cs_fact_campaign
-- set actual_incremental_sales_service = final.actual_incremental_sales_service
--   ,actual_cost_service = final.actual_cost_service
--   ,actual_incremental_sales_bws = final.actual_incremental_sales_bws
--   ,actual_incremental_sales_big_w = final.actual_incremental_sales_big_w
--   ,actual_incremental_sales_supers = final.actual_incremental_sales_supers
--   ,actual_cost_supers = final.actual_cost_supers
--   ,actual_cost_big_w = final.actual_cost_big_w
--   ,actual_cost_bws = final.actual_cost_bws
-- from (
--
--         select *
--        from loyalty_bi_analytics.cs_fact_campaign
--        where entry_type = 'Actuals'
-- ) final
--
-- where cs_fact_campaign.campaign_id = final.campaign_id
-- and cs_fact_campaign.fw_end_day_id = final.fw_end_day_id
-- and cs_fact_campaign.entry_type != 'Actuals'
-- ;

;
update loyalty_bi_analytics.cs_fact_campaign
        set forecasting_all_cost_supers = forecasting_manual_points_cost_supers + forecasting_non_manual_points_cost_supers + forecasting_social_media_cost_supers + forecasting_programmatic_media_cost_supers + forecasting_direct_mail_cost_supers + forecasting_printing_cost_supers + forecasting_inventory_cost_supers
           ,forecasting_all_cost_bws = forecasting_manual_points_cost_bws + forecasting_non_manual_points_cost_bws + forecasting_social_media_cost_bws + forecasting_programmatic_media_cost_bws + forecasting_direct_mail_cost_bws + forecasting_printing_cost_bws + forecasting_inventory_cost_bws
           ,forecasting_all_cost_big_w = forecasting_manual_points_cost_big_w + forecasting_non_manual_points_cost_big_w + forecasting_social_media_cost_big_w + forecasting_programmatic_media_cost_big_w + forecasting_direct_mail_cost_big_w + forecasting_printing_cost_big_w + forecasting_inventory_cost_big_w
           ,forecasting_all_cost_services = forecasting_manual_points_cost_services + forecasting_non_manual_points_cost_services + forecasting_social_media_cost_services + forecasting_programmatic_media_cost_services + forecasting_direct_mail_cost_services + forecasting_printing_cost_services + forecasting_inventory_cost_services
           ,forecasting_all_cost_member_programs = forecasting_manual_points_cost_member_programs + forecasting_non_manual_points_cost_member_programs + forecasting_social_media_cost_member_programs + forecasting_programmatic_media_cost_member_programs + forecasting_direct_mail_cost_member_programs + forecasting_printing_cost_member_programs + forecasting_inventory_cost_member_programs




;
truncate table loyalty_bi_analytics.cs_financial_campaign_cost;

insert into loyalty_bi_analytics.cs_financial_campaign_cost
select b.day_id
       ,case when a.banner = 'super' then 16
              when a.banner = 'bws' then 24
                     when a.banner = 'bigw' then 28
                            end as division_id
,a.forecast_cost
,a.budget_cost

from (select fw_end_date,-forecast_cost as forecast_cost,-budget_cost as budget_cost,'super' as banner
from loyalty_bi_analytics.cd_cost_budget
union all
select fw_end_date,-forecast_cost as forecast_cost,-budget_cost as budget_cost,'bws' as banner
from loyalty_bi_analytics.cd_cost_budget_bws
union all
select fw_end_date,-forecast_cost as forecast_cost,-budget_cost as budget_cost,'bigw' as banner
from loyalty_bi_analytics.cd_cost_budget_bigw) a
       inner join loyalty_bi_analytics.cs_dim_date b
       on trunc(a.fw_end_date) = trunc(b.fw_end_date)

;
truncate loyalty_bi_analytics.cs_dim_date_period;


insert into loyalty_bi_analytics.cs_dim_date_period(fw_end_date, day_id, time_period, fyyear)
with latest_financial_week as (

    select distinct trunc(dd.fw_end_date) as fw_end_date,dd.fy,dd.hofy,dd.qofy,dd.pofy, 'FY'+right(dd.fy::varchar,2) as FY_Year
    from loyalty.dim_date dd inner join (
        SELECT trunc(fw_end_date) as fw_end_date, dd.fy, dd.qofy, dd.pofy, dd.hofy, dd.wofy
        FROM loyalty.dim_date dd
        WHERE dd.clndr_date = trunc(dateadd(DAY, -6, getdate()))
    ) b
    on dd.qofy = b.qofy
    and dd.pofy = b.pofy
    and dd.hofy = b.hofy
    and dd.wofy = b.wofy
    and (dd.fy = b.fy or dd.fy = b.fy - 1)
)

, final as (
  select b.fw_end_date,b.day_id,'YTD' as time_period,FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date >= b.fw_end_date
  and a.fy = b.fy
UNION ALL
      select b.fw_end_date,b.day_id,'QTD',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date >= b.fw_end_date
  and (a.fy::varchar+(case when len(a.qofy) = 1 then '0'+a.qofy::varchar else a.qofy::varchar end)::varchar)::int = b.qofy
  and a.fy = b.fy
union ALL
      select b.fw_end_date,b.day_id,'MTD',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date >= b.fw_end_date
  and (a.fy::varchar+(case when len(a.pofy) = 1 then '0'+a.pofy::varchar else a.pofy::varchar end)::varchar)::int = b.pofy
  and a.fy = b.fy
union ALL
      select b.fw_end_date,b.day_id,'HTD',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date >= b.fw_end_date
  and (a.fy::varchar+(case when len(a.hofy) = 1 then '0'+a.hofy::varchar else a.hofy::varchar end)::varchar)::int = b.hofy
  and a.fy = b.fy
union ALL
select b.fw_end_date,b.day_id,'FY'+a.fy::varchar,FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  a.fy = b.fy
UNION ALL
    select b.fw_end_date,b.day_id,'PQ',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.qofy = 1 then (a.fy-1)::varchar(4)+'0'+ 4::VARCHAR(1) else a.fy::varchar(4)+'0'+ (a.qofy-1)::varchar(1) end = b.qofy
UNION ALL
  select b.fw_end_date,b.day_id,'PM',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.pofy = 1 then (a.fy-1)::varchar(4)+'12'::VARCHAR(1)
      when a.pofy > 10 then a.fy::varchar(4) + (a.pofy-1)::varchar(1)
      when a.pofy <= 10 then  a.fy::varchar(4)+'0'+ (a.pofy-1) end = b.pofy
UNION all

  select b.fw_end_date,b.day_id,'PW',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date = b.fw_end_date
union all
     select b.fw_end_date,b.day_id,'PH',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.hofy = 1 then (a.fy-1)::varchar(4)+'0'+ 2::VARCHAR(1) else a.fy::varchar(4)+'0'+ (a.hofy-1)::varchar(1) end = b.hofy
)

select *
from final;
select *
from loyalty_bi_analytics.cs_dim_date_period
where time_period = 'HTD'
;
update loyalty_bi_analytics.cs_dim_date_period
        set period_start_date = a.period_start_date,
          period_description = a.period_description,
          period_end_date = a.period_end_date
from (
select time_period,fyyear,(min(fw_end_date)-6)::date as period_start_date,
  max(fw_end_date)::date as period_end_date,
  case when time_period = 'YTD' then 'Year to Date'
       when time_period = 'HTD' then 'Half Year to Date'
       when time_period = 'QTD' then 'Quarter to Date'
       when time_period = 'MTD' then 'Month to Date'
       when time_period like 'FY%' then 'Full Financial Year'
       when time_period like 'PM' then 'Previous Month'
       when time_period like 'PW' then 'Previous Week'
       when time_period like 'PQ' then 'Previous Quater'
       when time_period = 'PH' then 'Previous Half' end as period_description
from loyalty_bi_analytics.cs_dim_date_period
group by time_period,fyyear) a
where cs_dim_date_period.time_period = a.time_period
and cs_dim_date_period.fyyear = a.fyyear;

update loyalty_bi_analytics.cs_fact_campaign
        set forecasting_incremental_sales_services = forecasting_incremental_sales_supers
where campaign_id like 'SER%'
and entry_type = 'Forecasting';
;
insert into loyalty_bi_analytics.cs_dim_date_period(fw_end_date, day_id, time_period, fyyear)
with latest_financial_week as (

   select distinct trunc(dd.fw_end_date) as fw_end_date,dd.fy,dd.hofy,dd.qofy,dd.pofy, 'FY'+right(dd.fy::varchar,2) as FY_Year
    from loyalty.dim_date dd inner join (
        SELECT trunc(fw_end_date) as fw_end_date, dd.fy, dd.qofy, dd.pofy, dd.hofy, dd.wofy
        FROM loyalty.dim_date dd
        WHERE dd.clndr_date = trunc(dateadd(DAY, -6, getdate()))
    ) b
    on dd.qofy = b.qofy
    and dd.pofy = b.pofy
    and dd.hofy = b.hofy
    and dd.wofy = b.wofy
    and (dd.fy = b.fy or dd.fy = b.fy - 1)
)


, final as (
  select b.fw_end_date,b.day_id,'YTC' as time_period,FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date < b.fw_end_date
  and a.fy = b.fy
    UNION ALL
    select b.fw_end_date,b.day_id,'NQ',FY_Year
       from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.qofy = 4 then (a.fy+1)::varchar(4)+'0'+ 1::VARCHAR(1) else a.fy::varchar(4)+'0'+ (a.qofy+1)::varchar(1) end = b.qofy
       UNION ALL
             select b.fw_end_date,b.day_id,'TQ',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  a.fy::varchar(4)+'0'+ (a.qofy)::varchar(1)  = b.qofy
                UNION ALL
             select b.fw_end_date,b.day_id,'TH',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  a.fy::varchar(4)+'0'+ (a.hofy)::varchar(1)  = b.hofy
          UNION ALL
             select b.fw_end_date,b.day_id,'NH',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.hofy = 2 then a.fy::varchar(4)+'0'+ (a.hofy+1)::varchar(1) else a.fy::varchar(4)+'0'+ (a.hofy+1)::varchar(1) end  = b.hofy

UNION ALL

select b.fw_end_date,b.day_id,'NM',FY_Year
  from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.pofy = 12 then (a.fy+1)::varchar(4)+'01'::VARCHAR(1)
      when a.pofy >= 9 then a.fy::varchar(4) + (a.pofy+1)::varchar(1)
      when a.pofy < 9 then  a.fy::varchar(4)+'0'+ (a.pofy+1) end = b.pofy
UNION all

  select b.fw_end_date,b.day_id,'NW',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date + 7  = b.fw_end_date

UNION ALL

  select b.fw_end_date,b.day_id,'N2W',FY_Year
       from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date + 14  = b.fw_end_date
  or a.fw_end_date + 7 = b.fw_end_date
union all
select b.fw_end_date,b.day_id,'N2M',FY_Year
  from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.pofy = 12 then (a.fy+1)::varchar(4)+'01'::VARCHAR(1)
      when a.pofy >= 9 then a.fy::varchar(4) + (a.pofy+1)::varchar(1)
      when a.pofy < 9 then  a.fy::varchar(4)+'0'+ (a.pofy+1)::varchar(1) end = b.pofy
or
      case when a.pofy = 12 then (a.fy+1)::varchar(4)+'02'::VARCHAR(1)
      when a.pofy = 11 then (a.fy+1)::varchar(4) + '01'::varchar(1)
      when a.pofy >= 8 then  a.fy::varchar(4) + (a.pofy+2)::varchar(1)
      when a.pofy < 8 then  a.fy::varchar(4)+'0'+ (a.pofy+2)::varchar(1) end = b.pofy

UNION ALL
         select b.fw_end_date,b.day_id,'N3M',FY_Year
  from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.pofy = 12 then (a.fy+1)::varchar(4)+'01'::VARCHAR(1)
      when a.pofy >= 9 then a.fy::varchar(4) + (a.pofy+1)::varchar(1)
      when a.pofy < 9 then  a.fy::varchar(4)+'0'+ (a.pofy+1)::varchar(1) end = b.pofy
or
      case when a.pofy = 12 then (a.fy+1)::varchar(4)+'02'::VARCHAR(1)
      when a.pofy = 11 then (a.fy+1)::varchar(4) + '01'::varchar(1)
      when a.pofy >= 8 then  a.fy::varchar(4) + (a.pofy+2)::varchar(1)
      when a.pofy < 8 then  a.fy::varchar(4)+'0'+ (a.pofy+2)::varchar(1) end = b.pofy
or
case when a.pofy = 12 then (a.fy+1)::varchar(4)+'03'::VARCHAR(1)
      when a.pofy = 11 then (a.fy+1)::varchar(4) + '02'::varchar(1)
       when a.pofy = 10 then (a.fy+1)::varchar(4) + '01'::varchar(1)
      when a.pofy >= 7 then  a.fy::varchar(4) + (a.pofy+3)::varchar(1)
      when a.pofy < 7 then  a.fy::varchar(4)+'0'+ (a.pofy+3)::varchar(1) end = b.pofy
union all

select b.fw_end_date,b.day_id,'N2Q',FY_Year
       from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.qofy = 4 then (a.fy+1)::varchar(4)+'0'+ 1::VARCHAR(1) else a.fy::varchar(4)+'0'+ (a.qofy+1)::varchar(1) end = b.qofy
  or  case when a.qofy = 3 then (a.fy+2)::varchar(4)+'0'+ 1::VARCHAR(1) else a.fy::varchar(4)+'0'+ (a.qofy+2)::varchar(1) end = b.qofy
UNION ALL
         select b.fw_end_date,b.day_id,'TM',FY_Year
from latest_financial_week a inner join loyalty_bi_analytics.cs_dim_date b
  on  case when a.pofy >= 10 then a.fy::varchar(4)+ (a.pofy)::varchar(1)
                             else a.fy::varchar(4) + '0' +(a.pofy)::varchar(1) end = b.pofy
)

select *
--into loyalty_bi_analytics.cs_dim_date_period
from final;


update loyalty_bi_analytics.cs_dim_date_period
        set period_start_date = a.period_start_date,
          period_description = a.period_description,
          period_end_date = a.period_end_date
from (
select time_period,fyyear,(min(fw_end_date)-6)::date as period_start_date,
  max(fw_end_date)::date as period_end_date,
  case when time_period = 'YTD' then 'Year to Date'
       when time_period = 'HTD' then 'Half Year to Date'
       when time_period = 'QTD' then 'Quarter to Date'
       when time_period = 'MTD' then 'Month to Date'
       when time_period like 'FY%' then 'Full Financial Year'
       when time_period like 'PM' then 'Previous Month'
       when time_period like 'PW' then 'Previous Week'
       when time_period like 'PQ' then 'Previous Quater'
        when time_period like 'NM' then 'Next Month'
       when time_period like 'NW' then 'Next Week'
       when time_period like 'NQ' then 'Next Quater'
       when time_period like 'YTC' then 'Year to Come'

       when time_period like 'TQ' then 'Current Quarter'
       when time_period like 'TH' then 'Current Half'
       when time_period like 'NH' then 'Next Half'
       when time_period like 'N2W' then 'Next Two Weeks'
        when time_period like 'N3M' then 'Next Three Months'
       when time_period like 'N2M' then 'Next Two Months'
       when time_period like 'N2Q' then 'Next Two Quarters'
       when time_period like 'TM' then 'Current Month'
       when time_period = 'PH' then 'Previous Half'

  end as period_description
from loyalty_bi_analytics.cs_dim_date_period
group by time_period,fyyear) a
where cs_dim_date_period.time_period = a.time_period
and cs_dim_date_period.fyyear = a.fyyear;

;
select *
from loyalty_bi_analytics.cs_dim_date
;
delete from loyalty_bi_analytics.cs_fact_campaign
where entry_type = 'Actuals'
and fw_end_day_id>=79
;

------Inserting the Actuals Campaign Inc Sales Value--------
insert into loyalty_bi_analytics.cs_fact_campaign(campaign_id,fw_end_day_id,number_of_contacts,actual_incremental_sales_supers,actual_incremental_sales_bws,actual_incremental_sales_big_w,promo_post,entry_type)
with actual_inc_sale as (
select nvl(nvl(a.campaign_code,b.campaign_code),c.campaign_code) as campaign_code,
  case when nvl(nvl(a.offer_exec_start_date,b.offer_exec_start_date),c.offer_exec_start_date) is null then nvl(a.fw_end_date,b.fw_end_date) else nvl(nvl(a.offer_exec_start_date,b.offer_exec_start_date),c.offer_exec_start_date) end as offer_exec_start_date,
  nvl(a.inc_sales,0) as super_inc_sales,
  nvl(b.inc_sales,0) as bws_incs_sales,
  nvl(c.inc_sales,0) as bigw_inc_sales,
  nvl(nvl(a.fw_end_date,b.fw_end_date),c.fw_end_date) fw_end_date,
  nvl(nvl(a.target_count,b.target_count),c.target_count) as number_of_contact,
  nvl(nvl(a.input_type,b.input_type),c.input_type) as input_type
  ,case when a.campaign_code is not null then 'Super' when b.campaign_code is not null then 'BWS' when c.campaign_code is not null then 'BIGW' end as source_type
from (select fw_end_date,campaign_code,campaign_name,offer_exec_start_date,input_type
,sum(inc_sales) as inc_sales,max(target_count) as target_count
from  loyalty_bi_analytics.vw_inc_sales_wkly_rprt
group by 1,2,3,4,5) a

FULL OUTER JOIN  (select fw_end_date,campaign_code,campaign_name,offer_exec_start_date,input_type
,sum(inc_sales) as inc_sales,max(target_count) as target_count
from  loyalty_bi_analytics.vw_inc_sales_wkly_rprt_bws
group by 1,2,3,4,5) b
  on a.fw_end_date = b.fw_end_date
  and a.campaign_code = b.campaign_code
  and a.offer_exec_start_date = b.offer_exec_start_date
full outer join (select fw_end_date,campaign_code,campaign_name,offer_exec_start_date,input_type
,sum(inc_sales) as inc_sales,max(target_count) as target_count
from  loyalty_bi_analytics.vw_inc_sales_wkly_rprt_bigw
group by 1,2,3,4,5) c
  on a.fw_end_date = c.fw_end_date
  and a.campaign_code = c.campaign_code
  and a.offer_exec_start_date = c.offer_exec_start_date)


select a.campaign_code + '_' + trunc(a.offer_exec_start_date)::text,b.day_id,a.number_of_contact
  ,a.super_inc_sales
  ,a.bws_incs_sales
  ,a.bigw_inc_sales
  ,case  when a.input_type like 'Post%' then 'post' else 'promo' end as input_type
  ,'Actuals'
  --,source_type
from actual_inc_sale a inner join loyalty_bi_analytics.cs_dim_date b
  on a.fw_end_date = b.fw_end_date
where  b.fw_end_date >='2018-06-24';


;

;
------Inserting the Actuals Campaign Cost-------------

update loyalty_bi_analytics.cs_fact_campaign
        set actual_cost_bws = cost.bws_actual_cost,
            actual_cost_big_w = cost.bigw_actual_cost,
          actual_cost_supers = cost.super_actual_cost
from ( with a as (

        SELECT
          nvl(nvl(super.campaign_code, bws.campaign_code), bigw.campaign_code)                            AS campaign_code,
          CASE WHEN nvl(nvl(super.campaign_start_date, bws.campaign_start_date), bigw.campaign_start_date) IS NULL
            THEN nvl(super.fw_end_date, bws.fw_end_date)
          ELSE nvl(nvl(super.campaign_start_date, bws.campaign_start_date),
                   bigw.campaign_start_date) END                                                          AS offer_exec_start_date,
          nvl(nvl(super.fw_end_date, bws.fw_end_date),
              bigw.fw_end_date)                                                                           AS fw_end_date,
          nvl(super.actual_cost,
              0)                                                                                          AS super_actual_cost,
          nvl(bws.actual_cost,
              0)                                                                                          AS bws_actual_cost,
          nvl(bigw.actual_cost,
              0)                                                                                          AS bigw_actual_cost
        FROM (
               SELECT
                 fw_end_date,
                 dc_campaign_code       AS campaign_code,
                 dc_campaign_start_date AS campaign_start_date,
                 sum(cost)              AS actual_cost
               FROM loyalty_bi_analytics.cd_campaign_summary_report_manual
               WHERE dc_campaign_start_date >= '2018-05-01'
               GROUP BY 1, 2, 3) super

          FULL OUTER JOIN (
                            SELECT
                              fw_end_date,
                              dc_campaign_code       AS campaign_code,
                              dc_campaign_start_date AS campaign_start_date,
                              sum(cost)              AS actual_cost
                            FROM loyalty_bi_analytics.cd_campaign_summary_report_manual_bws
                            WHERE dc_campaign_start_date >= '2018-05-01'
                            GROUP BY 1, 2, 3) bws
            ON super.campaign_code = bws.campaign_code
               AND super.campaign_start_date = bws.campaign_code
               AND super.fw_end_date = bws.fw_end_date
          FULL OUTER JOIN
          (
            SELECT
              fw_end_date,
              dc_campaign_code       AS campaign_code,
              dc_campaign_start_date AS campaign_start_date,
              sum(cost)              AS actual_cost
            FROM loyalty_bi_analytics.cd_campaign_summary_report_manual_bigw
            WHERE dc_campaign_start_date >= '2018-05-01'
            GROUP BY 1, 2, 3) bigw
            ON super.campaign_code = bigw.campaign_code
               AND super.campaign_start_date = bigw.campaign_code
               AND super.fw_end_date = bigw.fw_end_date
)
select cdd.day_id,a.campaign_code+'_'+a.offer_exec_start_date as campaign_id,a.bigw_actual_cost,a.bws_actual_cost,a.super_actual_cost
from a inner join loyalty_bi_analytics.cs_dim_date cdd
  on a.fw_end_date = cdd.fw_end_date) cost
where cs_fact_campaign.campaign_id = cost.campaign_id
and cs_fact_campaign.fw_end_day_id = cost.day_id
;


----Insert into Dim Campaign where there is a campaign code in Forecasting but unmatched Campaign Start Date----
Insert into loyalty_bi_analytics.cs_dim_campaign
with actual_inc_sale as (
  select split_part(campaign_id,'_',1) as campaign_code
  ,case when len(split_part(campaign_id,'_',2)) = 10 then split_part(campaign_id,'_',2)::date else null end as offer_exec_start_date
  ,campaign_id
  from loyalty_bi_analytics.cs_fact_campaign
where campaign_id not in (
  select campaign_id
  from loyalty_bi_analytics.cs_dim_campaign
)
and entry_type = 'Actuals'
)

select
  distinct a.campaign_code + '_' +trunc(a.offer_exec_start_date)::text
,a.campaign_code
,a.offer_exec_start_date::date
,c.campaign_name_description
,c.campaign_duration
,c.post_campaign_duration
,c.channel_id
,c.division_id
,c.stream_id
,c.campaign_type_id
,c.source_table
,c.exe_status
from actual_inc_sale a inner join (select campaign_code,max(campaign_name_description) as campaign_name_description,max(campaign_duration) as campaign_duration
,max(post_campaign_duration) as post_campaign_duration, max(channel_id) as channel_id
,max(division_id) as division_id, max(stream_id) as stream_id, max(campaign_type_id) as campaign_type_id,'Manual' as source_table,True as exe_status
from loyalty_bi_analytics.cs_dim_campaign
group by 1) c
  on a.campaign_code = c.campaign_code
where campaign_id in (select campaign_id from loyalty_bi_analytics.cs_dim_campaign);

----Insert into Dim Campaign where campaign code in not in forecasting, find from loyalty_bi_analytics.dim_campaign----
insert into loyalty_bi_analytics.cs_dim_campaign
with actual_inc_sale as (
  select split_part(campaign_id,'_',1) as campaign_code
  ,case when len(split_part(campaign_id,'_',2)) = 10 then split_part(campaign_id,'_',2)::date else null end as offer_exec_start_date
  ,campaign_id
  ,actual_incremental_sales_supers
  ,actual_incremental_sales_bws
  ,actual_incremental_sales_big_w
  ,actual_incremental_sales_service
  ,number_of_contacts
  from loyalty_bi_analytics.cs_fact_campaign
where campaign_id not in (
  select campaign_id
  from loyalty_bi_analytics.cs_dim_campaign
)
and entry_type = 'Actuals'
)

select distinct a.campaign_id
  ,a.campaign_code
  ,a.offer_exec_start_date::date
  ,nvl(right(dc.campaign_desc,280),'no description')::varchar(280)
  ,nvl(dc.campaign_duration_weeks,1)
  ,nvl(dc.post_campaign_weeks,0)
  , case when lower(dc.channel) = 'rewardshub' then 129
     when lower(dc.channel) in ('mail','dm') then 1025
     when lower(dc.channel) in ('fb','facebook') then 16385
     when lower(dc.channel) = 'email' then 32769
     when lower(dc.channel) = 'docket' then 513
     when lower(dc.channel) = 'display' then 2049
     else  32769 end as channel_id
  ,case when a.actual_incremental_sales_bws !=0 or a.campaign_code like 'BCV%' or a.campaign_code like 'BCT%' then 24
        when a.actual_incremental_sales_big_w !=0 or a.campaign_code like 'WCV%' or a.campaign_code like 'WCT%' then 28
        when a.campaign_code like 'SER%' then 30
        else 16
   end as division_id
,nvl(cds.stream_id,1)
,1
,'Manual'
,True
  --,case when a.input_type like 'Promo%' then 'promo' when a.input_type like 'Post%' then 'post' end as input_type
  --,'Actuals'
from actual_inc_sale a
  left join loyalty_bi_analytics.dim_campaign dc
  on a.campaign_code = dc.campaign_code
  and a.offer_exec_start_date = dc.campaign_start_date
  and dc.week_nbr = 1
  left join loyalty_bi_analytics.cs_dim_stream cds
  on split_part(a.campaign_code,'-',1) = cds.stream
where a.campaign_id not in (select campaign_id from loyalty_bi_analytics.cs_dim_campaign);
-- and b.fw_end_date >='2018-07-01'
-- where a.campaign_code + '_' +trunc(a.offer_exec_start_date)::text
-- not in  (select campaign_id from loyalty_bi_analytics.cs_dim_campaign)
-- and b.fw_end_date >='2018-07-01';
;

update loyalty_bi_analytics.cs_dim_campaign
set channel_id = case when lower(dc.channel) = 'rewardshub' then 129
     when lower(dc.channel) in ('mail','dm') then 1025
     when lower(dc.channel) in ('fb','facebook') then 16385
     when lower(dc.channel) = 'email' then 32769
     when lower(dc.channel) = 'docket' then 513
     when lower(dc.channel) = 'display' then 2049
     else  32769 end
from loyalty_bi_analytics.dim_campaign dc
where source_table = 'Manual'
and cs_dim_campaign.campaign_code = dc.campaign_code
and trunc(cs_dim_campaign.offer_start_date) = trunc(dc.campaign_start_date)

;

update loyalty_bi_analytics.cs_dim_campaign
set channel_id = case when lower(dc.channel) = 'rewardshub' then 129
     when lower(dc.channel) in ('mail','dm') then 1025
     when lower(dc.channel) in ('fb','facebook') then 16385
     when lower(dc.channel) = 'email' then 32769
     when lower(dc.channel) = 'docket' then 513
     when lower(dc.channel) = 'display' then 2049
     else  32769 end
from loyalty_bi_analytics.dim_campaign dc
where (cs_dim_campaign.channel_id = 4096 or cs_dim_campaign.channel_id = 0)
and cs_dim_campaign.campaign_code = dc.campaign_code
and trunc(cs_dim_campaign.offer_start_date) = trunc(dc.campaign_start_date)


;

----Insert into Dim Campaign where campaign code in not in forecasting, not in dim_campaign----

insert into loyalty_bi_analytics.cs_dim_campaign
with actual_inc_sale as (
  select split_part(campaign_id,'_',1) as campaign_code
  ,case when len(split_part(campaign_id,'_',2)) = 10 then split_part(campaign_id,'_',2)::date else null end as offer_exec_start_date
  ,campaign_id
  ,actual_incremental_sales_supers
  ,actual_incremental_sales_bws
  ,actual_incremental_sales_big_w
  ,actual_incremental_sales_service
  ,number_of_contacts
  from loyalty_bi_analytics.cs_fact_campaign
where campaign_id not in (
  select campaign_id
  from loyalty_bi_analytics.cs_dim_campaign
)
and entry_type = 'Actuals'
)

select distinct a.campaign_id
  ,a.campaign_code
  ,a.offer_exec_start_date::date
  ,'no description'
  ,1
  ,0
  ,32769
  ,case when a.actual_incremental_sales_bws !=0 or a.campaign_code like 'BCV%' or a.campaign_code like 'BCT%' then 24
        when a.actual_incremental_sales_big_w !=0 or a.campaign_code like 'WCV%' or a.campaign_code like 'WCT%' then 28
        when a.campaign_code like 'SER%' then 30
        else 16
   end as division_id
,nvl(cds.stream_id,1)
,1
,'Manual'
,True
  --,case when a.input_type like 'Promo%' then 'promo' when a.input_type like 'Post%' then 'post' end as input_type
  --,'Actuals'
from actual_inc_sale a left join loyalty_bi_analytics.cs_dim_stream cds
  on split_part(a.campaign_code,'-',1) = cds.stream
;
---
truncate loyalty_bi_analytics.cs_fact_campaign_cost;
insert into loyalty_bi_analytics.cs_fact_campaign_cost
select case when division_desc = 'Supermarkets' then 16
            when division_desc = 'BIGW' then 28
            when division_desc = 'BWS' then 24
              end as division_id
  ,day_id,sum(cost) as cost_btl
from loyalty_bi_analytics.vw_cpp_final a
inner join loyalty_bi_analytics.cs_dim_date cdd
  on a.fwk = cdd.fw_end_date
  where category in ('BTL-LMS','BTL-Virtual','Manual Upload','Online')
  and division_desc not in ('Petrol','Metro')
group by a.division_desc,2;




insert into loyalty_bi_analytics.cs_fact_campaign(campaign_id,fw_end_day_id,actual_cost_service,actual_incremental_sales_service,promo_post,entry_type)

with service_inc_sales as (
select a.campaign_code,a.campaign_start_date,a.metric_name,a.week_nbr_sort_order,(right(column_period,4)+'-'+substring(right(column_period,10),4,2)+'-'+substring(right(column_period,10),1,2))::date as fw_end_date
  ,regexp_replace(a.value, '([^0-9.])','')::float as value

from loyalty_bi_analytics.fact_campaign_summary a

 -- and a.campaign_name = b.offer_start_date
where a.metric_name in ('CLV','Cost Per Conversion','Number of Targeted Conversion')
and a.week_nbr_sort_order <100
and a.campaign_code like 'SER%'
)

select a.campaign_code+'_'+ trunc(a.campaign_start_date),d.day_id,nvl(a.value*b.value,0.00) as cost,nvl(c.value,0.00) as inc_sale
,'promo','Forecasting'
from service_inc_sales a
  inner join service_inc_sales b
  on a.campaign_code = b.campaign_code
  and a.campaign_start_date = b.campaign_start_date
  and a.week_nbr_sort_order = b.week_nbr_sort_order
  inner join service_inc_sales c
  on a.campaign_code = c.campaign_code
  and a.campaign_start_date = c.campaign_start_date
  and a.week_nbr_sort_order = c.week_nbr_sort_order
  inner join loyalty_bi_analytics.cs_dim_date d
  on CASE WHEN date_part(DOW, a.fw_end_date) BETWEEN 1 AND 4
              THEN next_day(a.fw_end_date, 'Sunday')
            ELSE next_day(a.fw_end_date + 6, 'Sunday') END= d.fw_end_date
where a.metric_name = 'Cost Per Conversion'
and b.metric_name = 'Number of Targeted Conversion'
and c.metric_name = 'CLV'
;


insert into loyalty_bi_analytics.cs_fact_campaign(campaign_id,fw_end_day_id,actual_cost_service,actual_incremental_sales_service,promo_post,entry_type)

with service_inc_sales as (
select a.campaign_code,a.campaign_start_date,a.metric_name,a.week_nbr_sort_order,(right(column_period,4)+'-'+substring(right(column_period,10),4,2)+'-'+substring(right(column_period,10),1,2))::date as fw_end_date
  ,regexp_replace(a.value, '([^0-9.])','')::float as value

from loyalty_bi_analytics.fact_campaign_summary a

 -- and a.campaign_name = b.offer_start_date
where a.metric_name in ('CLV','Cost Per Conversion','Number of Targeted Conversion')
and a.week_nbr_sort_order <100
and a.campaign_code like 'SER%'
)

select a.campaign_code+'_'+ trunc(a.campaign_start_date),d.day_id,nvl(a.value*b.value,0.00) as cost,nvl(c.value,0.00) as inc_sale
,'promo','Actuals'
from service_inc_sales a
  inner join service_inc_sales b
  on a.campaign_code = b.campaign_code
  and a.campaign_start_date = b.campaign_start_date
  and a.week_nbr_sort_order = b.week_nbr_sort_order
  inner join service_inc_sales c
  on a.campaign_code = c.campaign_code
  and a.campaign_start_date = c.campaign_start_date
  and a.week_nbr_sort_order = c.week_nbr_sort_order
  inner join loyalty_bi_analytics.cs_dim_date d
  on CASE WHEN date_part(DOW, a.fw_end_date) BETWEEN 1 AND 4
              THEN next_day(a.fw_end_date, 'Sunday')
            ELSE next_day(a.fw_end_date + 6, 'Sunday') END= d.fw_end_date
where a.metric_name = 'Cost Per Conversion'
and b.metric_name = 'Number of Targeted Conversion'
and c.metric_name = 'CLV'

;


update loyalty_bi_analytics.cs_fact_campaign
        set campaign_target_count = targeted,
          campaign_delivered_count = delivered,
          campaign_open_count = opened,
          campaign_unsub_count = unsubed
from (
with qualified_campaigns as (
select distinct split_part(campaign_id,'_',1) as campaign_code
      ,case when len(split_part(campaign_id,'_',2)) = 10 then split_part(campaign_id,'_',2)::date else null end  as campaign_start_date
from loyalty_bi_analytics.cs_fact_campaign cfc
where entry_type = 'Actuals'
and actual_incremental_sales_supers !=0 )
,
campaign_aggregation_data_1 AS (
  SELECT
    fcss.campaign_start_date,
    fcss.campaign_code,
    count(DISTINCT deliver_flag)       AS delivered,
    count(DISTINCT crn)                AS targeted,
    count(DISTINCT open_flag)          AS opened,
    count(DISTINCT fcss.unsub_flag)    AS unsubed
  FROM loyalty_bi_analytics.fact_campaign_sales fcss
    INNER JOIN qualified_campaigns qc
      ON fcss.campaign_code = qc.campaign_code
         AND fcss.campaign_start_date = qc.campaign_start_date
  WHERE fcss.campaign_audience_type = 'T'
        AND fcss.special_sales IS NULL
  GROUP BY fcss.campaign_start_date, fcss.campaign_code

  UNION ALL

  SELECT
    fcss.campaign_start_date,
    fcss.campaign_code,
    count(DISTINCT deliver_flag)     AS delivered,
    count(DISTINCT crn)              AS targeted,
    count(DISTINCT open_flag)        AS opened,
    count(DISTINCT fcss.unsub_flag)  AS unsubed
  FROM loyalty_bi_analytics.fact_campaign_sales_weekly_saver fcss
    INNER JOIN qualified_campaigns qc
      ON fcss.campaign_code = qc.campaign_code
         AND fcss.campaign_start_date = qc.campaign_start_date
  WHERE fcss.campaign_audience_type = 'T'
        AND fcss.special_sales IS NULL
  GROUP BY fcss.campaign_start_date, fcss.campaign_code, 1

  UNION ALL

  SELECT
    fcss.campaign_start_date,
    fcss.campaign_code,
    count(DISTINCT deliver_flag)       AS delivered,
    count(DISTINCT crn)                AS targeted,
    count(DISTINCT open_flag)          AS opened,
    count(DISTINCT fcss.unsub_flag)    AS unsubed
  FROM loyalty_bi_analytics.fact_campaign_sales_solus fcss
    INNER JOIN qualified_campaigns qc
      ON fcss.campaign_code = qc.campaign_code
         AND fcss.campaign_start_date = qc.campaign_start_date
  WHERE fcss.campaign_audience_type = 'T'
  GROUP BY fcss.campaign_start_date, fcss.campaign_code


    UNION ALL

  SELECT
    fcss.campaign_start_date,
    fcss.campaign_code,
    count(DISTINCT deliver_flag)     AS delivered,
    count(DISTINCT crn)              AS targeted,
    count(DISTINCT open_flag)        AS opened,
    count(DISTINCT fcss.unsub_flag)  AS unsubed
  FROM loyalty_bi_analytics.fact_campaign_sales_services fcss
    INNER JOIN qualified_campaigns qc
      ON fcss.campaign_code = qc.campaign_code
         AND fcss.campaign_start_date = qc.campaign_start_date
  WHERE fcss.campaign_audience_type = 'T'
        AND fcss.special_sales IS NULL
  GROUP BY fcss.campaign_start_date, fcss.campaign_code, 1

  UNION ALL

  SELECT
    fcss.campaign_start_date,
    fcss.campaign_code,
    count(DISTINCT deliver_flag)     AS delivered,
    count(DISTINCT crn)              AS targeted,
    count(DISTINCT open_flag)        AS opened,
    count(DISTINCT fcss.unsub_flag)  AS unsubed
  FROM loyalty_bi_analytics.fact_campaign_sales_online fcss
    INNER JOIN qualified_campaigns qc
      ON fcss.campaign_code = qc.campaign_code
         AND fcss.campaign_start_date = qc.campaign_start_date
  WHERE fcss.campaign_audience_type = 'T'
        AND fcss.special_sales IS NULL
  GROUP BY fcss.campaign_start_date, fcss.campaign_code, 1
)

select *
from campaign_aggregation_data_1) campaign_engagement_data
where cs_fact_campaign.campaign_id = campaign_engagement_data.campaign_code+'_'+campaign_engagement_data.campaign_start_date
;

with temp as (
select *,row_number() over (PARTITION BY  campaign_id) as row_no
from loyalty_bi_analytics.cs_dim_campaign
where campaign_id in (
  SELECT campaign_id
  FROM loyalty_bi_analytics.cs_dim_campaign
  GROUP BY campaign_id
  HAVING count(*) > 1
))
select *
into loyalty_bi_analytics.cs_dim_campaign_temp
from temp
where row_no = 1
;
delete from loyalty_bi_analytics.cs_dim_campaign
where campaign_id in (
  SELECT campaign_id
  FROM loyalty_bi_analytics.cs_dim_campaign
  GROUP BY campaign_id
  HAVING count(*) > 1)
;
insert into loyalty_bi_analytics.cs_dim_campaign
        select campaign_id,campaign_code,offer_start_date,campaign_name_description,campaign_duration,post_campaign_duration,channel_id,division_id,stream_id,campaign_type_id,source_table,campaign_count_flag,member_program_id
from loyalty_bi_analytics.cs_dim_campaign_temp;

drop table  loyalty_bi_analytics.cs_dim_campaign_temp
;



UPDATE loyalty_bi_analytics.cs_dim_campaign
        set division_id = 24
where campaign_id = 'BCV-1801_2018-08-10';

;





truncate table loyalty_bi_analytics.cs_fact_member

;

;
insert  into loyalty_bi_analytics.cs_fact_member
 select
-- case when dd.fw_end_date >= '2-Jul-2017' and dd.fw_end_date < getDate() -7 then 'TYTD'
-- when dd.fw_end_date >= '2-Jul-2017' - 7 * 52 and dd.fw_end_date < getDate() -7 - 7 * 52 then 'LYTD'
-- end as period
-- , campaign_stream, campaign_main_type
-- , sum(inc_sales) as inc_sales
-- , sum(cost) as cost
     campaign_code+'_'+campaign_start_date as campaign_id
    ,cdd.day_id
 , sum(inc_sales_email) as inc_sales_email_super
 , sum(target_count) as contact_count_super
 , 0 as inc_sales_email_bws
 , 0 as contact_count_bws
 , 0 as inc_sale_email_bigw
 , 0 as contact_count_bigw
 , 0 as inc_sales_service
 , 0 as contact_count_service
 , sum(unsub_count) as unsub_count_super
 , 0 as unsub_count_bws
 , 0 as unsub_count_bigw
 , 0 as unsub_count_service
 , 'Actuals'
 from
 (
  select fw_end_date, dc.campaign_code, dc.campaign_start_date,
  sum(case when c.campaign_code  is not null then target_count*1.0/no_weeks end) as target_count,
  sum(case when c.campaign_code  is not null then ubsub_count*1.0/no_weeks end ) as unsub_count,
  sum(incremental_sales) as inc_sales,
  sum(cost) as cost ,

  sum(case when c.campaign_code  is not null then incremental_sales end) as inc_sales_email
  from (
   select fw_end_date, dc_campaign_code, dc_campaign_start_date, incremental_sales, cost, max(week_nbr) over(partition by dc_campaign_code, dc_campaign_start_date) as no_weeks from (
      select *
      --, dense_rank() over(partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as no_week from (
      , row_number() over (partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as week_nbr from (
       select distinct fw_end_date, dc_campaign_code, dc_campaign_start_date, sum(incremental_sales) as incremental_sales, sum(cost) as cost
       from loyalty_bi_analytics.cd_campaign_summary_report_manual
       where incremental_sales is not null and incremental_sales <> 0 group by 1,2,3)
   )
  )
  a
  inner join
  (
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count , count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales
   where campaign_audience_type = 'T'
   --and campaign_code = 'CVM-0180'
   group by 1,2

   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count, count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_weekly_saver
   where campaign_audience_type = 'T' group by 1,2
   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_solus
   where campaign_audience_type = 'T' group by 1,2
     union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_services
   where campaign_audience_type = 'T' group by 1,2
       union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_online
   where campaign_audience_type = 'T' group by 1,2

  ) b
  on a.dc_campaign_code = b.campaign_code
  and a.dc_campaign_start_date = b.campaign_start_date
  inner join (select distinct campaign_code, campaign_start_date, campaign_stream, campaign_main_type from loyalty_bi_analytics.dim_campaign) dc
  on a.dc_campaign_code = dc.campaign_code
  and a.dc_campaign_start_date = dc.campaign_start_date

  left join (select distinct campaign_code, offer_exec_start_date from loyalty.et_resp_sendlog) c
  on a.dc_campaign_code = c.campaign_code
  and a.dc_campaign_start_date = c.offer_exec_start_date
  where incremental_sales <> 0 and incremental_sales is not null
  group by 1,2,3
 ) a
/* inner join (select distinct fy, pofy, fw_end_date from dim_date) dd
 on a.fw_end_date =trunc(dd.fw_end_date)
   inner join loyalty_bi_analytics.cs_dim_date cdd
   on trunc(a.fw_end_date) = trunc(cdd.fw_end_date)
   inner join (select distinct day_id  from loyalty_bi_analytics.cs_dim_date_period) f
   on cdd.day_id = f.day_id
   inner join loyalty_bi_analytics.cs_dim_date_period cddp
   on cdd.fw_end_date = cddp.fw_end_date
   inner join loyalty_bi_analytics.cs_dim_campaign cdc
   on a.campaign_code+'_'+a.campaign_start_date = cdc.campaign_id
   inner join loyalty_bi_analytics.cs_dim_channel channel
   on cdc.channel_id = channel.channel_id
  where dd.fy::int >= 2018
   and a.inc_sales_email is not null
 group by 1,2
*/
 inner join (select distinct fy, pofy, fw_end_date from dim_date) dd
 on a.fw_end_date =trunc(dd.fw_end_date)
inner join loyalty_bi_analytics.cs_dim_date cdd
   on trunc(a.fw_end_date) = trunc(cdd.fw_end_date)
    inner join (select distinct day_id  from loyalty_bi_analytics.cs_dim_date_period) f
   on cdd.day_id = f.day_id
  where dd.fy::int >= 2018
   and a.inc_sales_email is not null
 group by 1,2

;




;


insert into loyalty_bi_analytics.cs_fact_member
select
-- case when dd.fw_end_date >= '2-Jul-2017' and dd.fw_end_date < getDate() -7 then 'TYTD'
-- when dd.fw_end_date >= '2-Jul-2017' - 7 * 52 and dd.fw_end_date < getDate() -7 - 7 * 52 then 'LYTD'
-- end as period
-- , campaign_stream, campaign_main_type
-- , sum(inc_sales) as inc_sales
-- , sum(cost) as cost
   campaign_code+'_'+campaign_start_date as campaign_id
    ,cdd.day_id
 , 0 as inc_sales_email_super
 , 0 as contact_count_super
 , sum(inc_sales_email) as inc_sales_email_bws
 , sum(target_count) as contact_count_bws
 , 0 as inc_sale_email_bigw
 , 0 as contact_count_bigw
 , 0 as inc_sales_service
 , 0 as contact_count_service
 , 0 as unsub_count_super
 , sum(unsub_count) as unsub_count_bws
 , 0 as unsub_count_bigw
 , 0 as unsub_count_service
 , 'Actuals'
 from
 (
  select fw_end_date, dc.campaign_code, dc.campaign_start_date,
  sum(case when c.campaign_code  is not null then target_count*1.0/no_weeks end) as target_count,
  sum(case when c.campaign_code  is not null then ubsub_count*1.0/no_weeks end ) as unsub_count,
  sum(incremental_sales) as inc_sales,
  sum(cost) as cost ,

  sum(case when c.campaign_code  is not null then incremental_sales end) as inc_sales_email
  from (
   select fw_end_date, dc_campaign_code, dc_campaign_start_date, incremental_sales, cost, max(week_nbr) over(partition by dc_campaign_code, dc_campaign_start_date) as no_weeks from (
      select *
      --, dense_rank() over(partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as no_week from (
      , row_number() over (partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as week_nbr from (
       select distinct fw_end_date, dc_campaign_code, dc_campaign_start_date, sum(incremental_sales) as incremental_sales, sum(cost) as cost
       from loyalty_bi_analytics.cd_campaign_summary_report_manual_bws
       where incremental_sales is not null and incremental_sales <> 0 and dc_campaign_code+'_'+dc_campaign_start_date not in (select distinct campaign_id from loyalty_bi_analytics.cs_fact_member ) group by 1,2,3
    )
   )
  )
  a
  inner join
  (
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count , count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales
   where campaign_audience_type = 'T'
   --and campaign_code = 'CVM-0180'
   group by 1,2

   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count, count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_weekly_saver
   where campaign_audience_type = 'T' group by 1,2
   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_solus
   where campaign_audience_type = 'T' group by 1,2
     union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_services
   where campaign_audience_type = 'T' group by 1,2
       union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_online
   where campaign_audience_type = 'T' group by 1,2

  ) b
  on a.dc_campaign_code = b.campaign_code
  and a.dc_campaign_start_date = b.campaign_start_date
  inner join (select distinct campaign_code, campaign_start_date, campaign_stream, campaign_main_type from loyalty_bi_analytics.dim_campaign) dc
  on a.dc_campaign_code = dc.campaign_code
  and a.dc_campaign_start_date = dc.campaign_start_date

  left join (select distinct campaign_code, offer_exec_start_date from loyalty.et_resp_sendlog) c
  on a.dc_campaign_code = c.campaign_code
  and a.dc_campaign_start_date = c.offer_exec_start_date
  where incremental_sales <> 0 and incremental_sales is not null
  group by 1,2,3
 ) a
 inner join (select distinct fy, pofy, fw_end_date from dim_date) dd
 on a.fw_end_date =trunc(dd.fw_end_date)
inner join loyalty_bi_analytics.cs_dim_date cdd
   on trunc(a.fw_end_date) = trunc(cdd.fw_end_date)
    inner join (select distinct day_id  from loyalty_bi_analytics.cs_dim_date_period) f
   on cdd.day_id = f.day_id
  where dd.fy::int >= 2018
   and a.inc_sales_email is not null
 group by 1,2


;

insert into loyalty_bi_analytics.cs_fact_member
select
-- case when dd.fw_end_date >= '2-Jul-2017' and dd.fw_end_date < getDate() -7 then 'TYTD'
-- when dd.fw_end_date >= '2-Jul-2017' - 7 * 52 and dd.fw_end_date < getDate() -7 - 7 * 52 then 'LYTD'
-- end as period
-- , campaign_stream, campaign_main_type
-- , sum(inc_sales) as inc_sales
-- , sum(cost) as cost
   campaign_code+'_'+campaign_start_date as campaign_id
     ,cdd.day_id
 , 0 as inc_sales_email_super
 , 0 as contact_count_super
 , 0 as inc_sales_email_bws
 , 0 as contact_count_bws
 , sum(inc_sales_email) as inc_sale_email_bigw
 , sum(target_count) as contact_count_bigw
 , 0 as inc_sales_service
 , 0 as contact_count_service
 , 0 as unsub_count_super
 , 0 as unsub_count_bws
 , sum(unsub_count) as unsub_count_bigw
 , 0 as unsub_count_service
 , 'Actuals'
 from
 (
  select fw_end_date, dc.campaign_code, dc.campaign_start_date,
  sum(case when c.campaign_code  is not null then target_count*1.0/no_weeks end) as target_count,
  sum(case when c.campaign_code  is not null then ubsub_count*1.0/no_weeks end ) as unsub_count,
  sum(incremental_sales) as inc_sales,
  sum(cost) as cost ,

  sum(case when c.campaign_code  is not null then incremental_sales end) as inc_sales_email
  from (
   select fw_end_date, dc_campaign_code, dc_campaign_start_date, incremental_sales, cost, max(week_nbr) over(partition by dc_campaign_code, dc_campaign_start_date) as no_weeks from (
      select *
      --, dense_rank() over(partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as no_week from (
      , row_number() over (partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as week_nbr from (
       select distinct fw_end_date, dc_campaign_code, dc_campaign_start_date, sum(incremental_sales) as incremental_sales, sum(cost) as cost
       from loyalty_bi_analytics.cd_campaign_summary_report_manual_bigw
       where incremental_sales is not null and incremental_sales <> 0 and dc_campaign_code+'_'+dc_campaign_start_date not in (select distinct campaign_id from loyalty_bi_analytics.cs_fact_member ) group by 1,2,3
    )
   )
  )
  a
  inner join
  (
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count , count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales
   where campaign_audience_type = 'T'
   --and campaign_code = 'CVM-0180'
   group by 1,2

   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count, count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_weekly_saver
   where campaign_audience_type = 'T' group by 1,2
   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_solus
   where campaign_audience_type = 'T' group by 1,2
     union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_services
   where campaign_audience_type = 'T' group by 1,2
       union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_online
   where campaign_audience_type = 'T' group by 1,2

  ) b
  on a.dc_campaign_code = b.campaign_code
  and a.dc_campaign_start_date = b.campaign_start_date
  inner join (select distinct campaign_code, campaign_start_date, campaign_stream, campaign_main_type from loyalty_bi_analytics.dim_campaign) dc
  on a.dc_campaign_code = dc.campaign_code
  and a.dc_campaign_start_date = dc.campaign_start_date

  left join (select distinct campaign_code, offer_exec_start_date from loyalty.et_resp_sendlog) c
  on a.dc_campaign_code = c.campaign_code
  and a.dc_campaign_start_date = c.offer_exec_start_date
  where incremental_sales <> 0 and incremental_sales is not null
  group by 1,2,3
 ) a
 inner join (select distinct fy, pofy, fw_end_date from dim_date) dd
 on a.fw_end_date =trunc(dd.fw_end_date)
   inner join loyalty_bi_analytics.cs_dim_date cdd
   on trunc(a.fw_end_date) = trunc(cdd.fw_end_date)
    inner join (select distinct day_id  from loyalty_bi_analytics.cs_dim_date_period) f
   on cdd.day_id = f.day_id
  where dd.fy::int >= 2018
   and a.inc_sales_email is not null
 group by 1,2


;
update loyalty_bi_analytics.cs_fact_member
        set inc_sales_email_bws = a.inc_sales_email_bws,
          contact_count_bws = a.contact_count_bws,
          unsub_count_bws = a.unsub_count_bws
from (select
-- case when dd.fw_end_date >= '2-Jul-2017' and dd.fw_end_date < getDate() -7 then 'TYTD'
-- when dd.fw_end_date >= '2-Jul-2017' - 7 * 52 and dd.fw_end_date < getDate() -7 - 7 * 52 then 'LYTD'
-- end as period
-- , campaign_stream, campaign_main_type
-- , sum(inc_sales) as inc_sales
-- , sum(cost) as cost
   campaign_code+'_'+campaign_start_date as campaign_id
 , 0 as inc_sales_email_super
 , 0 as contact_count_super
 , sum(inc_sales_email) as inc_sales_email_bws
 , sum(target_count) as contact_count_bws
 , 0 as inc_sale_email_bigw
 , 0 as contact_count_bigw
 , 0 as inc_sales_service
 , 0 as contact_count_service
 , 0 as unsub_count_super
 , sum(unsub_count) as unsub_count_bws
 , 0 as unsub_count_bigw
 , 0 as unsub_count_service
 from
 (
  select fw_end_date, dc.campaign_code, dc.campaign_start_date,
  sum(case when c.campaign_code  is not null then target_count*1.0/no_weeks end) as target_count,
  sum(case when c.campaign_code  is not null then ubsub_count*1.0/no_weeks end ) as unsub_count,
  sum(incremental_sales) as inc_sales,
  sum(cost) as cost ,

  sum(case when c.campaign_code  is not null then incremental_sales end) as inc_sales_email
  from (
   select fw_end_date, dc_campaign_code, dc_campaign_start_date, incremental_sales, cost, max(week_nbr) over(partition by dc_campaign_code, dc_campaign_start_date) as no_weeks from (
      select *
      --, dense_rank() over(partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as no_week from (
      , row_number() over (partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as week_nbr from (
       select distinct fw_end_date, dc_campaign_code, dc_campaign_start_date, sum(incremental_sales) as incremental_sales, sum(cost) as cost
       from loyalty_bi_analytics.cd_campaign_summary_report_manual_bws
       where incremental_sales is not null and incremental_sales <> 0 group by 1,2,3
    )
   )
  )
  a
  inner join
  (
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count , count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales
   where campaign_audience_type = 'T'
   --and campaign_code = 'CVM-0180'
   group by 1,2

   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count, count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_weekly_saver
   where campaign_audience_type = 'T' group by 1,2
   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_solus
   where campaign_audience_type = 'T' group by 1,2
     union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_services
   where campaign_audience_type = 'T' group by 1,2
       union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_online
   where campaign_audience_type = 'T' group by 1,2

  ) b
  on a.dc_campaign_code = b.campaign_code
  and a.dc_campaign_start_date = b.campaign_start_date
  inner join (select distinct campaign_code, campaign_start_date, campaign_stream, campaign_main_type from loyalty_bi_analytics.dim_campaign) dc
  on a.dc_campaign_code = dc.campaign_code
  and a.dc_campaign_start_date = dc.campaign_start_date

  left join (select distinct campaign_code, offer_exec_start_date from loyalty.et_resp_sendlog) c
  on a.dc_campaign_code = c.campaign_code
  and a.dc_campaign_start_date = c.offer_exec_start_date
  where incremental_sales <> 0 and incremental_sales is not null
  group by 1,2,3
 ) a
 inner join (select distinct fy, pofy, fw_end_date from dim_date) dd
 on a.fw_end_date =trunc(dd.fw_end_date)
  where dd.fy::int >= 2018
   and a.inc_sales_email is not null
 group by 1) a
where cs_fact_member.campaign_id = a.campaign_id
;
update loyalty_bi_analytics.cs_fact_member
        set inc_sale_email_bigw = a.inc_sale_email_bigw,
          contact_count_bigw = a.contact_count_bigw,
          unsub_count_bigw = a.unsub_count_bigw
from (select
-- case when dd.fw_end_date >= '2-Jul-2017' and dd.fw_end_date < getDate() -7 then 'TYTD'
-- when dd.fw_end_date >= '2-Jul-2017' - 7 * 52 and dd.fw_end_date < getDate() -7 - 7 * 52 then 'LYTD'
-- end as period
-- , campaign_stream, campaign_main_type
-- , sum(inc_sales) as inc_sales
-- , sum(cost) as cost
    campaign_code+'_'+campaign_start_date as campaign_id
 , 0 as inc_sales_email_super
 , 0 as contact_count_super
 , 0 as inc_sales_email_bws
 , 0 as contact_count_bws
 , sum(inc_sales_email) as inc_sale_email_bigw
 , sum(target_count) as contact_count_bigw
 , 0 as inc_sales_service
 , 0 as contact_count_service
 , 0 as unsub_count_super
 , 0 as unsub_count_bws
 , sum(unsub_count) as unsub_count_bigw
 , 0 as unsub_count_service
 from
 (
  select fw_end_date, dc.campaign_code, dc.campaign_start_date,
  sum(case when c.campaign_code  is not null then target_count*1.0/no_weeks end) as target_count,
  sum(case when c.campaign_code  is not null then ubsub_count*1.0/no_weeks end ) as unsub_count,
  sum(incremental_sales) as inc_sales,
  sum(cost) as cost ,

  sum(case when c.campaign_code  is not null then incremental_sales end) as inc_sales_email
  from (
   select fw_end_date, dc_campaign_code, dc_campaign_start_date, incremental_sales, cost, max(week_nbr) over(partition by dc_campaign_code, dc_campaign_start_date) as no_weeks from (
      select *
      --, dense_rank() over(partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as no_week from (
      , row_number() over (partition by dc_campaign_code, dc_campaign_start_date order by fw_end_date) as week_nbr from (
       select distinct fw_end_date, dc_campaign_code, dc_campaign_start_date, sum(incremental_sales) as incremental_sales, sum(cost) as cost
       from loyalty_bi_analytics.cd_campaign_summary_report_manual_bigw
       where incremental_sales is not null and incremental_sales <> 0 group by 1,2,3
    )
   )
  )
  a
  inner join
  (
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count , count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales
   where campaign_audience_type = 'T'
   --and campaign_code = 'CVM-0180'
   group by 1,2

   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count, count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_weekly_saver
   where campaign_audience_type = 'T' group by 1,2
   union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_solus
   where campaign_audience_type = 'T' group by 1,2
     union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_services
   where campaign_audience_type = 'T' group by 1,2
       union all
   select campaign_code, campaign_start_date, count(distinct deliver_flag) as target_count,count(distinct unsub_flag) as ubsub_count
   from loyalty_bi_analytics.fact_campaign_sales_online
   where campaign_audience_type = 'T' group by 1,2

  ) b
  on a.dc_campaign_code = b.campaign_code
  and a.dc_campaign_start_date = b.campaign_start_date
  inner join (select distinct campaign_code, campaign_start_date, campaign_stream, campaign_main_type from loyalty_bi_analytics.dim_campaign) dc
  on a.dc_campaign_code = dc.campaign_code
  and a.dc_campaign_start_date = dc.campaign_start_date

  left join (select distinct campaign_code, offer_exec_start_date from loyalty.et_resp_sendlog) c
  on a.dc_campaign_code = c.campaign_code
  and a.dc_campaign_start_date = c.offer_exec_start_date
  where incremental_sales <> 0 and incremental_sales is not null
  group by 1,2,3
 ) a
 inner join (select distinct fy, pofy, fw_end_date from dim_date) dd
 on a.fw_end_date =trunc(dd.fw_end_date)

   and a.inc_sales_email is not null
 group by 1) a
where cs_fact_member.campaign_id = a.campaign_id
;
-- delete from loyalty_bi_analytics.cs_fact_member
-- where campaign_id not in (select campaign_id from loyalty_bi_analytics.cs_dim_campaign);


insert  into loyalty_bi_analytics.cs_fact_member
select  b.campaign_id
     ,b.day_id
 , sum(a.forecasting_incremental_sales_supers) as inc_sales_email_super
 , sum(b.contact_count_super) as contact_count_super
 , 0 as inc_sales_email_bws
 , 0 as contact_count_bws
 , 0 as inc_sale_email_bigw
 , 0 as contact_count_bigw
 , 0 as inc_sales_service
 , 0 as contact_count_service
 , sum(b.contact_count_super) as unsub_count_super
 , 0 as unsub_count_bws
 , 0 as unsub_count_bigw
 , 0 as unsub_count_service
 , 'Forecasting'
  from loyalty_bi_analytics.cs_fact_campaign a
inner join loyalty_bi_analytics.cs_fact_member b
on a.campaign_id = b.campaign_id
    where a.entry_type = 'Forecasting'
group by 1,2;

-- update loyalty_bi_analytics.cs_fact_campaign
--         set fw_end_day_id = 9999
-- where campaign_id like 'SER%';

update loyalty_bi_analytics.cs_dim_campaign
        set campaign_count_flag = 1
where campaign_code not in ('BWS-XSHP','MEE-0001')
;

update loyalty_bi_analytics.cs_dim_campaign
        set campaign_count_flag = 0
where campaign_code in ('BWS-XSHP','MEE-0001');
;

;

-- delete
-- from loyalty_bi_analytics.cs_dim_division
-- where division_id = 31

;




update loyalty_bi_analytics.cs_dim_campaign
        set source_table = 'cross_banner'
from loyalty_bi_analytics.forecasting_cross_banner a
where cs_dim_campaign.campaign_code = a.campaign_code;


update loyalty_bi_analytics.cs_dim_campaign
        set division_id = a.division_id
from (

select a.campaign_id,c.division_id
from (select cfc.campaign_id,max(cfc.forecasting_incremental_sales_big_w) as bigw,max(cfc.forecasting_incremental_sales_supers) as super,max(cfc.forecasting_incremental_sales_bws) as bws
from loyalty_bi_analytics.cs_fact_campaign cfc
  inner join loyalty_bi_analytics.cs_dim_campaign cdc
  on cfc.campaign_id = cdc.campaign_id
where cdc.source_table = 'cross_banner'
group by 1) a inner join loyalty_bi_analytics.cs_dim_division c
on case when a.super>0 then True else False end  = c.supermarket
  and case when a.bws>0 then True else False end = c.bws
  and case when a.bigw>0 then True else False end = c.bigw
  and False = c.services
  and False = c.member_program) a
where cs_dim_campaign.campaign_id = a.campaign_id;




update loyalty_bi_analytics.cs_dim_campaign
        set campaign_duration = 8
where campaign_duration>8;


insert into loyalty_bi_analytics.cs_dim_stream
with temp as (
  select distinct split_part(campaign_code, '-', 1) as stream
  from loyalty_bi_analytics.cs_dim_campaign
  where split_part(campaign_code, '-', 1) not in (select stream
                                                  from loyalty_bi_analytics.cs_dim_stream)
)

select row_number() over (order by stream) + max_stream_id,stream
from temp
cross join (select max(cs_dim_stream.stream_id) max_stream_id
                                                  from loyalty_bi_analytics.cs_dim_stream);

update loyalty_bi_analytics.cs_dim_campaign
set stream_id = a.stream_id
from loyalty_bi_analytics.cs_dim_stream a
where cs_dim_campaign.stream_id = 1
and split_part(cs_dim_campaign.campaign_code,'-',1) = a.stream
;

update loyalty_bi_analytics.cs_dim_campaign
set division_id = 31,
    campaign_count_flag = false
where source_table in ('caltex')
;
update loyalty_bi_analytics.cs_dim_campaign
set division_id = 32,
    campaign_count_flag = false
where source_table in ('fuelco')
;
update loyalty_bi_analytics.cs_dim_campaign
set division_id = 30,
    campaign_count_flag = false
where source_table in ('services')
;
update loyalty_bi_analytics.cs_dim_campaign
set division_id = 1
where source_table = 'cross_banner';

update loyalty_bi_analytics.cs_dim_campaign
set division_id = 28
where offer_start_date>='2019-07-01'
and campaign_code like 'CNA%'
and campaign_name_description like 'Big W%'
;

update loyalty_bi_analytics.cs_fact_campaign
set campaign_counter = null
where campaign_id  in (
select distinct campaign_id
    from loyalty_bi_analytics.cs_dim_campaign
    where source_table in ('fuelco','caltex','services')
)
;


update loyalty_bi_analytics.cs_dim_campaign
set campaign_name_description = dc.campaign_name
from loyalty_bi_analytics.dim_campaign dc
where cs_dim_campaign.campaign_id = dc.campaign_code+'_'+dc.campaign_start_date
;

-- update loyalty_bi_analytics.cs_dim_campaign
-- set campaign_type_id = 3
-- where campaign_type_id = 1 or campaign_type_id = 0
;

--update loyalty_bi_analytics.cs_dim_campaign--
--set channel_id = 2048
--where channel_id = 4096
;
update loyalty_bi_analytics.cs_dim_campaign
set campaign_count_flag = False
where source_table = 'member'
;
update loyalty_bi_analytics.cs_fact_campaign
set actual_cost_supers = 0,
    actual_cost_big_w = 0,
    actual_cost_bws = 0
where entry_type = 'Forecasting';

update loyalty_bi_analytics.cs_dim_campaign
set cross_banner_flag = case when source_table = 'cross_banner' then 1 else 0 end;


update loyalty_bi_analytics.cs_fact_campaign
set promo_post = 'promo'
where campaign_id in (
select distinct a.campaign_id
from loyalty_bi_analytics.cs_fact_campaign a
inner join loyalty_bi_analytics.cs_dim_campaign b
on a.campaign_id = b.campaign_id
inner join loyalty_bi_analytics.cs_dim_division c
on b.division_id = c.division_id
where a.fw_end_day_id > 78
and (c.bws = False and c.bigw = false and c.supermarket = false))

;
update loyalty_bi_analytics.cs_fact_campaign
set promo_post = 'promo'
where fw_end_day_id >= 70
and campaign_id in (
select campaign_id
from (
select campaign_id,max(promo_post) as promo_post
from (
         select distinct campaign_id, promo_post
         from loyalty_bi_analytics.cs_fact_campaign
     )  a
group by campaign_id
having count(*) = 1) b
where b.promo_post = 'post')
and entry_type = 'Forecasting'
;

update loyalty_bi_analytics.cs_dim_campaign
set source_table = a.forecasting_sheet_source_table
from (select cs_fact_campaign.campaign_id, forecasting_sheet_source_table
     from loyalty_bi_analytics.cs_fact_campaign
     where entry_type = 'Forecasting'
     and fw_end_day_id >= 75
     and forecasting_sheet_source_table is not null) a
where a.campaign_id = cs_dim_campaign.campaign_id
;

update loyalty_bi_analytics.cs_dim_campaign
set division_id = case when source_table = 'cross_banner' then 1
                       when source_table = 'super' then 16
                      when source_table = 'lcp_cda' then 16
when source_table = 'lcp_bau' then 16
when source_table = 'cartology' then 16
when source_table = 'bws_de' then 24
when source_table = 'bws' then 24
when source_table = 'bws_blc' then 24
when source_table = 'bigw' then 28
when source_table = 'services' then 30
when source_table = 'caltex' then 31
when source_table = 'fuelco' then 32 end
where source_table in (
    'cross_banner',
'super',
'lcp_cda',
'lcp_bau',
'cartology',
'bws_de',
'bws',
'bws_blc',
'bigw',
'services',
'caltex',
'fuelco'

    )
;
update loyalty_bi_analytics.cs_dim_campaign
set source_table = 'Manual'
where campaign_id in
      (select distinct campaign_id
from loyalty_bi_analytics.cs_dim_campaign
where offer_start_date >= '2019-06-01'
and campaign_id not in (  select cs_fact_campaign.campaign_id
     from loyalty_bi_analytics.cs_fact_campaign
     where entry_type = 'Forecasting'
     and fw_end_day_id >= 75)
and source_table != 'Manual')
;

update loyalty_bi_analytics.cs_dim_campaign
set all_campaign_count_flag = FALSE
where source_table = 'Manual';



update loyalty_bi_analytics.cs_dim_campaign
set all_campaign_count_flag = TRUE
where source_table != 'Manual';



